# Pääkaupunkiseudun pyöräilyinfran lähteillä

Kävin osana [pyöräpysäköintipaikkojen kapasiteetin
täydennystä](https://www.openstreetmap.org/user/Barro/diary/402411)
läpi Helsingin kaupungin pyörätelineitä ja HSL:n
kaupunkipyöräasemien tietoja paikkaillessani kapasiteettipuutteita
OpenStreetMapissa. Tämä sisälsi OpenStreetMapista riippumattoman
karttadatan käsittelyä ja tämä artikkeli on lyhyt katsaus siihen, että
miten tätä voi hyödyntää, kun maastosta tarkistaa asioita.

## Helsingin kaupungin pyörätelineet karttapalvelusta

[Helsingin karttapalvelusta](https://kartta.hel.fi/) löytää kaupungin
vastuulla olevien pyörätelineiden sijainnin, toisin kuin
[Espoon](https://kartat.espoo.fi/) ja
[Vantaan](https://kartta.vantaa.fi/) vastaavista. Tämä antaa Helsingin
osalta hyvän pohjan OpenStreetMapin datasta löytyvien telineiden
sijaintitiedon täydentämiseen.

Helsingin karttapalvelusta saa otettua esimerkiksi
[GML-muotoisen](https://www.w3.org/Mobile/posdep/GMLIntroduction.html)
viennin pyörätelineiden koordinaateista. Tässä sijaintieto on
[GK25FIN-muodossa](https://epsg.io/3879), joka täytyy erikseen muuttaa
yleisemmin kansainvälisessä käytössä olevaan
[WGS84-koordinaatistoon](https://epsg.io/4326), joka on myös
OpenStreetMapissa käytössä oleva koordinaatisto. Lähdin jo hieman
tutkimaan geomatiikan saloja, jotta saisin laskettua itse nämä
muunnokset, mutta into lopahti ja käytin sen sijaan valmista
[pyproj](https://pypi.org/project/pyproj/)-kirjastoa näihin
koordinaatistomuunnoksiin.

OpenStreetMapin datasta pyörätelineet tuli haettua suhteellisen
yksinkertaisella
[Overpass-kyselyllä](https://overpass-turbo.eu/s/1FGq):

<pre><code>node
  [amenity=bicycle_parking]
  (60.13416, 24.67598, 60.26536, 25.1223);
out;
way
  [amenity=bicycle_parking]
  (60.13416, 24.67598, 60.26536, 25.1223);
(._;>;);
out;</code></pre>

OpenStreetMapin datasta sitten tuli laskettua lähimmät pisteet
suhteessa Helsingin kaupungin dataan ja karsittua kaikki pois, joissa
Helsingin kaupungin datassa olevan pisteen etäisyys OpenStreetMapin
dataan verrattuna oli "liian lähellä". Tämä lukema hieman vaihteli
riippuen siitä, että miltä käsitelty data vaikutti sen jälkeen, kun
näitä puuttuvia pyörätelinepisteitä kävi tämän projektin aikana läpi.

<figure id="pyörätelineet-kartat-helsinki-fi-ei-openstreetmapissa">
<a href="https://barro.gitlab.io/osm-diary-files/pääkaupunkiseudun-pyöräilyinfran-lähteillä/pyörätelineet-kartat-helsinki-fi-ei-openstreetmapissa.webp"><img
src="https://barro.gitlab.io/osm-diary-files/pääkaupunkiseudun-pyöräilyinfran-lähteillä/pyörätelineet-kartat-helsinki-fi-ei-openstreetmapissa.webp"
alt="Helsingin kaupungin karttadatassa olevat OpenStreetMapista löytymättömät pyörätelineet"
title="Helsingin kaupungin karttadatassa olevat OpenStreetMapista löytymättömät pyörätelineet"
/></a>
<figcaption>
Kuva 1: suhteellisen loppuvaiheessa oleva kuva Helsingin kaupungin
karttapalvelun mukaisista pyörätelineistä, joille ei ole vastinetta
OpenStreetMapissa.
</figcaption>
</figure>

Näistä pyörätelinedatan eroista sai sitten luotua
[OsmAndiin](https://osmand.net/) [kuvan
1](#pyörätelineet-kartat-helsinki-fi-ei-openstreetmapissa) tapaisen
kartan puuttuvista pyörätelineistä. Tätä karttadataa pystyi sitten
käyttämään OsmAndin avulla navigointiin, kun kulki ympäriinsä näiden
puuttuvien telineiden perässä.

### Karttadatan yllättäviä hyötyjä

Sen ei pitäisi olla yllättävää, että kun tekee kartan
OpenStreetMapista puuttuvista pyörätelineistä, sellaisia tosiaan
löytyy. Itselläni tuli kuitenkin hieman yllätyksenä, että miten tietyt
pyörätelineiksi luokiteltavat ratkaisut oli jääneet aiemmin
huomaamatta.

<figure id="hyväntoivonpuisto-openstreetmap">
<a href="https://barro.gitlab.io/osm-diary-files/pääkaupunkiseudun-pyöräilyinfran-lähteillä/hyväntoivonpuisto-openstreetmap-2023-09-08.webp"><img
src="https://barro.gitlab.io/osm-diary-files/pääkaupunkiseudun-pyöräilyinfran-lähteillä/hyväntoivonpuisto-openstreetmap-2023-09-08.webp"
alt="Hyväntoivonpuiston pyörätelineet OpenStreetMapin mukaan"
title="Hyväntoivonpuiston pyörätelineet OpenStreetMapin mukaan"
/></a>
<figcaption>
Kuva 2: Hyväntoivonpuiston pyörätelineet OpenStreetMapin mukaan
(siniset ympyrät).
</figcaption>
</figure>

<figure id="hyväntoivonpuisto-kartat-helsinki-fi">
<a href="https://barro.gitlab.io/osm-diary-files/pääkaupunkiseudun-pyöräilyinfran-lähteillä/hyväntoivonpuisto-kartat-helsinki-fi-2023-09-08.webp"><img
src="https://barro.gitlab.io/osm-diary-files/pääkaupunkiseudun-pyöräilyinfran-lähteillä/hyväntoivonpuisto-kartat-helsinki-fi-2023-09-08.webp"
alt="Hyväntoivonpuiston pyörätelineet Helsingin kaupungin karttapalvelun mukaan"
title="Hyväntoivonpuiston pyörätelineet Helsingin kaupungin karttapalvelun mukaan"
/></a>
<figcaption>
Kuva 3: Hyväntoivonpuiston pyörätelineet Helsingin kaupungin
karttapalvelun mukaan (punaiset pisteet).</figcaption>
</figure>

Merkittävin yllättävä tapaus oli Hyväntoivonpuistossa
Ruoholahdessa. Kuvista [2](#hyväntoivonpuisto-openstreetmap) ja
[3](#hyväntoivonpuisto-kartat-helsinki-fi) voi vertailla miten
Helsingin kaupungin karttadatan pyörätelineet suhteutui
OpenStreetMapin dataan Hyväntoivonpuistossa. Olin jo katsastanut
Hyväntoivonpuiston alueen ilman Helsingin kaupungin karttaa usempaan
otteeseen aina välillä Jätkäsaaressa käydessäni. Tämän alueen
erikoisenmuotoiset pyörätelineet olivat kuitenkin jääneet minulta
havaitsematta. Nyt näiden tulisi olla kaikki OpenStreetMapissa, kun
tämän esimerkkikuvan ottamisen jälkeen tein vielä yhden
päivitysreissun tälle alueelle.

### Epätarkkuudet Helsingin kaupungin datassa

Helsingin kaupungin tarjoamassa pyörätelinedatassa on jonkin verran
epätarkkuuksia ja käyttöä haittaavia ongelmia. Yleisin epätarkkuus
lienee se, että telineiden sijainti kartalla ei tarkalleen ole se mitä
OpenStreetMapissa ja usein myös todellisuudessa. Jotta tämä ei olisi
suurempi ongelma, käytin yksinkertaisena heuristiikkana sitä, että
kasvatetaan vaadittavaa minimietäisyyttä Helsingin kaupungin datasta
saatavien pyörätelineiden ja OpenStreetMapista löytyvien
pyörätelineiden vastaavuuksiksi. Jos näitä pyörätelinepuutteita sattuu
olemaan alueella, jolla telineitä on yleisestikin tiheästi,
esimerkiksi risteysalueiden eri puolilla, niin jäi minulta
havaitsematta. Tämän ei pitäisi olla suuri ongelma käytännössä tätä
dataa hyödyntäville, sillä jos lähellä on useampi pyörätelinerykelmä,
niin kartassa jo yksi riittää tiedoksi, että tänne voi hyvillä mielin
pysäköidä pyöränsä.

### Löytymättömät pyörätelineet Helsingin kaupungin datassa

Helsingin kaupungin karttadataa hyödyntämällä tulee myös ongelma, että
jossain paikoissa ei vaan löydy (= minä en löydä) pyörätelineitä siltä
alueelta, jossa Helsingin kaupungin kartta väittää niitä olevan. Osa
telineistä myös on paikoissa, joissa oli tarkistushetkellä ilmestynyt
työmaa. En yhtään ihmettelisi, jos mylläyksen seurauksena
pyörätelinetieto jäisi joskus vanhentuneena Helsingin kaupungin
karttoihin.

Kaikki nämä Helsingin kaupungin kartan epätarkkuudet on vähän nihkeitä
sen suhteen, että näistä täytyisi pistää perään korjauspyyntöä
kaupungille ja toivoa, että jokainen käsittelijäketju oikeasti on
kiinnostunut näistä epätarkkuuksista. Toinen vaihtoehto on ylläpitää
erillistä tietokantaa siitä, että mikä on pielessä. En ole kuitenkaan
löytänyt hyvää keinoa tätä varten, joka olisi helppo säilyttää, jakaa
ja ylläpitää. Menin loppupeleissä minimivaivalla ja jätin kummankin
asian tekemättä.

## HSL:n kaupunkipyöräasemat

[HSL:n
kaupunkipyöräasemien](https://www.hsl.fi/kaupunkipyorat/helsinki)
erojen paikkaus oli toinen tällainen projekti, jolla pyrin parantamaan
OpenStreetMapissa olevaa dataa vastaamaan siihen, mitä maastossa
sattuu olemaan. Tämä olikin melkoisen paljon monimutkaisempi kuin
Helsingin kaupungin pyörätelinepuutteiden lisääminen, sillä
kaupunkipyöräasemat ja niiden ominaisuuksista kertova data muuttuu
jatkuvasti. HSL:n kaupunkipyöräasemien OpenStreetMapin ja virallisen
data eroja kertomaan oli jo valmiina viikon välein päivittyvä OpenStreetMap-wikin
[Fi:Suomi/Joukkoliikenne/HSL/citybikes](https://wiki.openstreetmap.org/wiki/Fi:Suomi/Joukkoliikenne/HSL/citybikes)-sivu. Tämä
ei kuitenkaan mutta se ei tarjonnut suoraa tukea sille, että datan
saisi näppärästi OsmAndiin, niin tähän piti tehdä oma kotikutoinen
ratkaisu.

HSL:n kaupunkipyöräjärjestelmässä dataa on helposti koneluettavassa
muodossa saatavilla parista lähteestä. [Helsingin Seudun Liikenteen
(HSL) kaupunkipyöräasemat
avoindata.fi-palvelussa](https://www.avoindata.fi/data/fi/dataset/hsl-n-kaupunkipyoraasemat)
tarjoaa muutaman kerran vuodessa päivittyvän listan
kaupunkipyöräasemista. Tämä päivitystahti on aivan liian harva
suhteessa siihen, että mitä tahtia kaupunkipyöräasemien ominaisuudet
muuttuu. Reaaliaikainen rajapinta HSL:n kaupunkipyöräasemien dataan
löytyy [Digitransit-palvelusta](https://digitransit.fi/). Tässä
haittapuolena on se, että sinne pitää erikseen rekisteröityä, jos
haluaa käyttää virallisia rajapintoja, eikä
[GraphiQL-käyttöliittymän](https://api.digitransit.fi/graphiql/hsl)
käyttämän rajapinnan takaisinmallinnus napostele.

OpenStreetMapista löytyvät Helsingin ja Espoon järjestelmien
kaupunkipyöräasemat löytää [seuraavanlaisella
Overpass-kyselyllä](https://overpass-turbo.eu/s/1FGB):

<pre><code>node
  ["amenity"="bicycle_rental"]["network"="Helsinki&Espoo"]
  (60.13535, 24.69452, 60.2943, 25.18753);
out;
way
  ["amenity"="bicycle_rental"]["network"="Helsinki&Espoo"]
  (60.13535, 24.69452, 60.2943, 25.18753);
(._;>;);
out;</code></pre>

Koska OpenStreetMapin
[`ref`](https://wiki.openstreetmap.org/wiki/Key:ref)-tietueeseen on
laitettu Digitransit-datan `stationId`-tietuetta vastaava
kaupunkipyöräaseman numero, niin kaupunkipyöräasemien erojen
tarkasteluun ei tarvitse aseman tietojen suhteen oikein minkäänlaista
heuristiikkaa. Keskityin korjaamaan kaupunkipyöräasemien
pyöräpaikkojen lukumäärää ja sijaintia, jotka on kumpikin suhteellisen
helposti maastossa havaittavia ominaisuuksia.

### HSL:n kaupunkipyörädatan virheellisyydet

Samalla kun korjasi OpenStreetMapin dataan kaupunkipyöräasemien
tietojen virheellisyyksiä, niin tuli myös annettua HSL:lle palautetta
virheistä heidän datassaan. Se olikin urakka, sillä erityisesti
kapasiteetin osalta eroavaisuuksia siihen, että mitä on
todellisuudessa kentältä löytyy, on suhteellisen paljon. Myös sijainti
vaihtelee, välillä aseman ollessa tien toisella puolella, välillä
enemmältikin.

<figure id="kaupunkipyöräasemat-sijainti-kapasiteettieroavat">
<a href="https://barro.gitlab.io/osm-diary-files/pääkaupunkiseudun-pyöräilyinfran-lähteillä/kaupunkipyöräasemat-sijainti-kapasiteettieroavat-2023-09-08.webp"><img
src="https://barro.gitlab.io/osm-diary-files/pääkaupunkiseudun-pyöräilyinfran-lähteillä/kaupunkipyöräasemat-sijainti-kapasiteettieroavat-2023-09-08.webp"
alt="Sijainti ja paikkojen lukumääräeroavaiset HSL:n kaupunkipyöräasemat kartalla"
title="Sijainti ja paikkojen lukumääräeroavaiset HSL:n kaupunkipyöräasemat kartalla"
/></a>
<figcaption>
Kuva 4: HSL:n kaupunkipyöräasemat, joiden sijainti (sininen) ja
paikkojen lukumäärä (pinkki) eroaa OpenStreetMapin datasta. Pääasiassa
HSL:n data poikkeaa siitä, mitä maastosta löytyi.
</figcaption>
</figure>

[Kuvassa 4](#kaupunkipyöräasemat-sijainti-kapasiteettieroavat) näkyy
syyskuun 2023 alkupuolella käyttämäni kaupunkipyöräasemien sijainti-
ja kapasiteettierojen visualisaatio OpenStreetMapin ja HSL:n datan
välillä. Palautteesta huolimatta, HSL ei korjaa dataansa aina
kuntoon. Tässä 4 kaupunkipyöräasemalla sijainti eroaa OpenStreetMapin
sijainnista. Näistä 3 sijaintia on varmasti muistini mukaan ollut
väärin HSL:n datassa, mutta en voi tietää ovatko he siirtäneet asemia
paikkaan, joka heidän tietokannassaan on kirjattu. Vai ovatko he vain
olleet reagoimatta antamaani palautteeseen. Mutta tämä ei tunnu olevan
kuitenkaan tilanne kaikissa raportoimissani tapauksissa, niin epäilen
tässä olevan jotain käsittelijä- ja aluekohtaista hajontaa siinä,
miten toimitaan.

Lisäksi 12 aseman kapasiteetti erosi. Näillä asemilla pyöräpaikat on 2
tai 4 paikan rykelmissä, joten kaikki joiden kapasiteetti on pariton,
on varmasti väärin. Näitä jostain syystä on ollut siellä täällä
jatkuvasti ja niitä tulee lisää. Onko tässä takana jokin juttu, että
HSL:n omasta tietokannasta tai käyttöliittymästä puuttuu keino merkitä
jotain tiettyä ominaisuutta ja tätä yleensä yksi vajaa -kapasiteettia
käytetään sitten johonkin tiedonvälitykseen? Vai onko kyse
laskuvirheestä, kun nämä lukumäärät yleensä poikkeaa yhdellä enemmän
mitä todellisuudessa? Mutta siinä tapauksessa luulisi +-1-jakauman
olevan tasaisempi. Nyt se pääasiassa on yhden paikan verran enemmän
mitä todellisuudessa. Luulin, että tässä voisi olla
näppäilyvirheistäkin kyse, kunnes havaitsin tapauksen, jossa oli 0 ja
1 numerot eroavina, eikä pelkästään tavanomaiset vierekkäiset
numerot. Joka tapauksessa näitä turhauttavia lukumääräerojakin on
tullut raportoitua ja ne vähän satunnaisesti korjaantuu tai ei
korjaannu HSL:n omaan dataan. Näitä paikkamäärävirheitä kuitenkin
syntyy kesken kaupunkipyöräkauden jatkuvasti lisää.

Loppujen lopuksi kaupunkipyöräasemien tietojen ajantasalla pitäminen
on vähän turhalta tuntuvaa taistelua. Nämä asemat kun
säännönmukaisesti muuttuu massoittain kauden alussa ja myöskin kesken
kauden. Lisäksi HSL:n palautejärjestelmä ei ole tehty sellaiseksi,
että siellä olisi mahdollista erikseen ilmoittaa heidän omassa
datassaan olevista virheistä. Riippunee käsittelijästä, että miten
nämä korjauspyynnöt välittyy kentälle ja tietokantapäivityskykyiselle
henkilölle.

## Loppupohdintoja

Jonkun toisen ylläpitämän datan pohjalta voi kertaalleen käydä asioita
läpi, mutta pidempiaikaisen ylläpidon toteuttamiseen tästä ei
ole. Epätarkka data pysyy näissä lähdekartoissa kunnes omistava taho
hoitaa sen kuntoon. Tämä taas riippuu siitä, että miten välitetty
palaute kulkee ja kiinnostaa dataa ylläpitävässä organisaatiossa.

Vaikka sama epätarkkuusongelma periaatteessa pätee myös
OpenStreetMapissa olevaan dataan, sitä sentään kuka tahansa pystyy
muuttamaan ilman pitkiä tiedonvälitysketjua.

Katson nyt kummankin projektin olevan omalta osaltani
paketissa ja en jatka näitä ensi kesänä. Helsingin kaupunki on
suunnitelmien mukaan rakentamassa vuosittain yli [900
pyöräpysäköintipaikkaa seuraavat pari
vuotta](https://www.hel.fi/static/liitteet/kaupunkiymparisto/julkaisut/julkaisut/julkaisu-31-20.pdf),
joten toivottavasti tilanne päätyy sellaiseksi, että tulevaisuudessa
ei tarvitse juurikaan turvautua tähän OpenStreetMapin dataan
löytääksen vähemmän tutuilla alueilla laadukkaan
pyöräpysäköintimahdollisuuden. Vaikkakaan tuo määrä vuodessa ei ole
hirveän paljon, sillä pienillä 4-5 telineen pyöräpysäköintialueilla
tuo tekee parhaimmillaankin luokkaa 100 erillistä
pyöräpysäköintialuetta vuodessa jaoteltuna koko Helsingin kaupungin
alueelle.
