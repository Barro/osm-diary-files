#!/usr/bin/env python3

import argparse
import os
import pathlib
import sys
import typing
import xml.etree.ElementTree as ET


def type_xml(value: str) -> ET.Element:
    print(f"Parsing {value}")
    return ET.parse(value).getroot()


def get_coordinates(element) -> ET.Element:
    if element.tag != "{http://www.topografix.com/GPX/1/1}wpt":
        return None
    lat = element.attrib.get("lat")
    lon = element.attrib.get("lon")
    if lat is None or lon is None:
        return None
    return "%0.7f" % round(float(lat), 7), "%0.7f" % round(float(lon), 7)


def main(argv: typing.List[str]) -> int:
    ET.register_namespace('', "http://www.topografix.com/GPX/1/1")
    ET.register_namespace("osmand", "https://osmand.net")
    parser = argparse.ArgumentParser(prog=argv[0])
    parser.add_argument("--out", type=pathlib.Path, required=True)
    parser.add_argument("gpx", type=type_xml, nargs="+")
    args = parser.parse_args(argv[1:])

    if len(args.gpx) < 2:
        print("Need more than 1 gpx file to merge")
        return os.EX_USAGE

    out = args.gpx[0]
    known_coordinates = set()
    for element in out.iter():
        coordinates = get_coordinates(element)
        if coordinates is not None:
            known_coordinates.add(coordinates)
        
    e_extensions = out.find("{http://www.topografix.com/GPX/1/1}extensions")
    e_points_groups = e_extensions.find("{https://osmand.net}points_groups")
    if e_points_groups is None:
        e_extensions = ET.SubElement(out, "extensions")
        e_points_groups = ET.SubElement(e_extensions, "osmand:points_groups")
    for xmltree in args.gpx[1:]:
        for element in xmltree.iter():
            if element.tag == "{http://www.topografix.com/GPX/1/1}group":
                e_points_groups.insert(-1, element)
                continue
            coordinates = get_coordinates(element)
            if coordinates is not None:
                if coordinates in known_coordinates:
                    continue
                known_coordinates.add(coordinates)
            else:
                continue
            out.insert(-1, element)

    with open(args.out, "wb") as fp:
        document = ET.ElementTree(out)
        ET.indent(document)
        document.write(
            fp,
            encoding="utf-8",
            xml_declaration=True,
            method="xml",
        )
    return os.EX_OK


if __name__ == "__main__":
    sys.exit(main(sys.argv))
