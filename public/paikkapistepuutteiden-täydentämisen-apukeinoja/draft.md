# Paikkapistepuutteiden täydentämisen apukeinoja

Taas jatkuu yksi kesä paikkapistepuutteiden täydentämisen osalta
samalla, kun tulee pyöräiltyä milloin minnekin. Työkalut itselläni
tähän
[mikrokartoitukseen](https://wiki.openstreetmap.org/wiki/Micromapping)
kehittyy ja optimoituu hiljalleen sen perusteella, mitä tehokkaampia
tapoja ja välineitä tehdä asioita tulee löydettyä.

## Laitteisto pyörän tangossa

OpenStreetMapin dataa on mahdollista päivittää mikrokartoitustasolla
kahdella eri lähestymistavalla:

* Paikan päällä kentällä. Tässä yleensä on rajattu aika mitä käyttää
  kussakin paikassa ja siten pitää valita se, mitä kirjaa ylös ja
  miten. Yleensä tässä työkalut rajoittaa, sillä kentällä harvemmin on
  muuta käytössä kuin mobiilikäyttöjärjestelmällä varustettu
  kannettava laite.
* Jälkikäteen muistiinpanojen perusteella. Tässä
  muistiinpanovälineistö rajoittaa sitä, että millaista tietoa ja
  kuinka paljon saa käyttöönsä siihen, kun jälkikäteen päivitää
  OpenStreetMapia.

Olen valinnut päivitystavaksi tehdä asiat paikan päällä kentällä
pyöräillessä ja vain poikkeustapauksissa jatkan kotona. [Viimekesäiset
varusteet](https://www.openstreetmap.org/user/Barro/diary/402039#varusteet)
on vaihtuneet kokonaan toisiin, tarjoten hieman paremman
kokemuksen. Nämä on nähtävissä [kuvasta 1](#pyörän-tangon-varustus).

<figure id="pyörän-tangon-varustus">
<a href="https://barro.gitlab.io/osm-diary-files/paikkapistepuutteiden-täydentämisen-apukeinoja/pyörän-tangon-varustus-2024.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapistepuutteiden-täydentämisen-apukeinoja/pyörän-tangon-varustus-2024.webp" height="400"
alt="Kuva pyörän tangosta, josta löytyy kaiutin, täppäri ja kännykkä."
title="Kuva pyörän tangosta, josta löytyy kaiutin, täppäri ja kännykkä."
/></a>
<figcaption>
Kuva 1: esimerkki kartoituksessa käytettävistä varusteista pyörän
tankoon kiinnitettynä kesällä 2024.
</figcaption>
</figure>

Tangon 8" täppäriksi päivitin tehokkaamman ja sateenkestävään
([Samsung Galaxy Tab Active 5](https://www.samsung.com/fi/business/tablets/galaxy-tab-active/galaxy-tab-active5-5g-sm-x306bzgaeeb/))-laitteeseen,
jotta ei tarvitsisi kärsiä niin paljoa suorituskyvyn puutteesta tai
vesisateesta. Tätä kyseistä laitetta mainostetaan kovaan käyttöön
sopivaksi, mutta aika näyttää miten on. Positiivisina puolina tässä on
ollut:

* Fyysiset näppäimet navigointiin. Tietää ainakin painaneensa
  näppäintä, kun tällä on selkeä tuntuma ja vaste.
* Sopivasti tyhjää tilaa reunoilla. Kädellä on helpompi ottaa
  laitteesta tukea, kun kämmen ei aiheuta reunoilla kontaktia itse
  käyttöliittymän kanssa.
* Sateenkestävyys. Navigointi sujuu tällä laitteella olosuhteissa kuin
  olosuhteissa.
* Laite toimii hanskojen kanssa. Paksuilla hanskoilla on turha
  kuvitella osuvansa yksityiskohtiin, mutta ohuemmilla hanskoilla
  pystyy tökkimään näyttöä ihan mukavasti.
* Laite on siedettävän tehoinen. Jumitilanteet
  [StreetCompletella](https://streetcomplete.app/) on suhteellisen
  harvinaisia ja 3 samanaikaista sovellusta pyörii ilman ongelmia.
* Näytön vajaan 1920x1200-kuvapisteen tarkkuus on riittävä 8" koolle
  ja käytetylle katseluetäisyydelle.
* Laitteessa on yksi ylimääräinen näppäin (Active Key), jonka voi
  asettaa avaamaan haluttuja sovelluksia lyhyellä ja pitkällä
  painalluksella erikseen. Nyt on 3 erillistä fyysisillä näppäimillä
  avautuvaa sovellusta käytössä.

Negatiivisina puolina tässä on ollut:

* Paikannustarkkuus tällä laitteella ei ole kovin kummoinen. Lienee
  suhteellisen halpoihin/keskitason puhelimiin suunnatun suunnattun
  Exynos 1380 piirisarjan ratkaisuja.
* Paikannuksen täydellinen sekoaminen ainakin kerran. Tähän auttoi
  laitteen uudelleenkäynnistys, joten jonkinlainen ohjelmisto-ongelma
  tässä oli sen suhteen.
* Akun kesto. Kylmällä, muutamassa plusasteessa, akkua kuluu helposti
  yli 1%/minuutti. Lämpimälläkin OsmAndin käyttö syö akun loppuun
  luokkaa 3 tunnin aikana.

Täppärin teline on RAM-kiinnikkeiden [C-sarjan
pallolla](https://rammount.com/products/ram-408-75-1u) ja [lyhyellä
varrella](https://rammount.com/products/ram-201u-b) varustettu
erikseen [Samsung Galaxy Tab Active -sarjan 8" laitteille tehty
teline](https://rammount.com/products/ram-hol-tab-sam29u).

Telineessä etuna aikaisempaan yleiskäyttöiseen [X-grip-tyypin
telineeseen](https://rammount.com/products/ram-hol-un8bcu) on se, että
täppärissä ei ole ole mitään näppäimien edessä haittaamassa
käyttöä. Myöskään X-gripin kumitassut ei tärinän seurauksena liiku
laitteen näppäimien päälle tai ole tiellä, kun fyysisiä näppäimiä
haluaa käyttää. Ja koska täppärin saa tästä telineestä poistettua
yhdellä kädellä vetämällä, kynnys täydentää StreetCompleten viestejä
kuvilla on laskenut rajusti.

Tätä nykyistä ratkaisua ennen tuli käytyä läpi myös RAM-kiinnikkeiden
B-koon pallo [keskipituisella
varrella](https://rammount.com/products/ram-b-201u), jossa ei
riittänyt enää kitkaa tähän painavampaan täppäriin. [Keskipituinen
C-koon varsi](https://rammount.com/products/ram-201u) oli myös
käytössä, mutta koska vähemmän tärinää aiheuttava lyhyt varsi myös
lopulta riitti, niin valitsin lyhyemmän varren.

## Overpass Turbo ja OsmAnd

[Overpass Turbo](https://overpass-turbo.eu/) tarjoaa kyselyrajapinnan
OpenStreetMapin dataan, jolla saa [Pyöräpysäköintiprojekti
kapasiteettitäydennysten osalta
pulkassa](https://www.openstreetmap.org/user/Barro/diary/402411)-artikkelin
mukaisesti kyseltyä kiinnostavia kohteita tarkastukseen. Tänä vuonna
otin lisäprojektiksi käydä pyöräparkit, joihin ei ole koskettu kuuteen
vuoteen, vuoden alusta mitettuna, ja päivittää muuttuneet
paikkapisteet ajantasaisiksi. Näitä paikkapisteitä keväällä kelien
lämmetessä oli melkoinen määrä, joten OsmAndin navigaatio-ominaisuudet
pääsi oikeuksiinsa läpikäyntejä suunnitellessa.

### OpenStreetMapin data OsmAndiin

Overpass Turboon kohdistuvat kyselyt tuottaa oletuksena dataa
XML-muodossa, joka ei ole suoraan hyödynnettävissä OsmAndissa. Tätä
hyödyntämistä avustamaan on tullut tehtyä pari skriptiä
([osm.py](osm.py), [convert-osm-to-gpx.py](convert-osm-to-gpx.py) ja
[merge-gpx-files.py](merge-gpx-files.py)), jotka muuntaa XML-muotoisen
datan GPX-muotoon, antaa paikkapisteille värin ja mahdollistaa
useamman tiedoston yhdistämisen. Tällä saadaan kaikki erilainen data
tuotua OsmAndiin yhden tiedoston kautta Favorites-osioon. Tämä
mahdollistaa suhteellisen tehokkaan OsmAndin datan päivityksen Google
Driven kautta.

### OsmAndin polut

OsmAndiin saa luotua helposti
[polkuja](https://osmand.net/docs/user/personal/tracks/), jotka
helpottaa merkittävästi paikkapisteiden välistä kulkemista. Näiden
polkujen tekeminen juuri ennen lähtöä on OsmAndin mobiiliversiolla sen
verran helppoa, että OsmAndin työpöytäselainversolla polkujen luominen
etukäteen on turhaa.

<figure id="osmand-polku-paksu">
<a href="https://barro.gitlab.io/osm-diary-files/paikkapistepuutteiden-täydentämisen-apukeinoja/osmand-reitti-visualisoitu-paksu.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapistepuutteiden-täydentämisen-apukeinoja/osmand-reitti-visualisoitu-paksu.webp" height="500"
alt="Polku OsmAndissa näyttää reitin paikkapisteiden välillä"
title="Polku OsmAndissa näyttää reitin paikkapisteiden välillä"
/></a>
<figcaption>
Kuva 2: polku OsmAndissa näyttää reitin paikkapisteiden välillä.
</figcaption>
</figure>

Piirretty polku kannattaa OsmAndissa säätää olemaan paksumpi, mitä
oletuksena, jotta se on helppo erottaa kaikesta muusta, mitä kartalla
näkyy. [Kuvassa 2](#osmand-polku-paksu) esimerkki siitä, miltä tämä
eri paikkapisterykelmien välille piirretty polku näyttää
kartalla. Tätä polun viivaa on sitten mukava seurailla, kun ei
tarvitse hirveämmin miettiä, että mihin kulkee seuraavaksi.

## OsmAnd ja tiheät karttapäivitykset

OsmAndin maksullinen versio
([OsmAnd+](https://osmand.net/docs/user/purchases/android/#osmand),
tai
[Maps+-ostos](https://osmand.net/docs/user/purchases/android/#maps))
tarjoaa kuukausittaisen karttadatan päivityksen OsmAndiin. Lisäksi
[OsmAnd Pro
-tilaus](https://osmand.net/docs/user/purchases/android/#osmand-pro)
mahdollistaa vaikka tunnin välein tehtävät karttapäivitykset.

Vähemmän mainostettu ominaisuus OsmAndissa on se, että tekemällä
[OpenStreetMap-muokkauksia](https://osmand.net/docs/user/plugins/osm-editing/)
suhteellisen säännöllisesti [saa myös tiheämmät karttapäivitykset
käyttöön](https://osmand.net/docs/user/plugins/osm-editing/#free-map-updates-for-mappers). Tämä
on erityisen hyödyllinen ominaisuus yksittäisten paikkapisteiden
lisäämisessä, sillä aina ei muutaman päivän jälkeen muista, että mitä
tarkalleen on lisännyt kartalle. Sitä välillä kun pyörii samoilla
alueilla eri tavoitteet mielessä.

OpenStreetMapin kuukausittainen muokkausmäärävaatimus yleensä tulee
täyteen huomaamatta, kiitos StreetCompleten. StreetCompletella
muokattavaa riittää tulevaisuudessakin, ellei tässä tule joku
StreetComplete-buumi ja tyhjentäisi kaikki käyttämäni tehtävät
lähialueilta. Toisaalta, se olisi positiivinen ongelma OpenStreetMapin
datan päivityksen suhteen.

### OsmAnd Pro -tilaus

Käytin jonkin aikaa [OsmAnd Pro
-tilausta](https://osmand.net/docs/user/purchases/android/) sillä
idealla, että saisin helpommin kopioitua useamman laitteeni välillä
dataa ja tehtyä myös reittejä käyttäen
työpöytäselainta. Valitettavasti OsmAnd Pro osoittautui
käyttökelvottomaksi useilta eri osin:

* Synkronointi ei tapahdu automaattisesti taustalla.
* Konfliktitilanteita eri laitteiden välillä ei ole kovinkaan hyvin
  hanskattu OsmAnd Prossa. Jatkuvasti tulee sellaista, jossa pitää
  valita, että mihin suuntaan synkronoidaan.
* OsmAnd kaatuu suurilla paikkapistemäärillä herkästi.
* OsmAnd-sovellus ja OsmAndin webbikäyttöliittymä kaatuu herkästi
  pidemmillä reiteillä. Erityisesti jos ne on kaiken lisäksi
  muokattuja. Tällä tavalla sain sovelluksen pariin otteeseen
  uudelleenasennuskuntoon.
* OsmAnd Prossa ei ole minkäänlaista järkevää käsittelymekanismia,
  mitä tulee rikkinäisiin asetuksiin. Nämä rikkinäiset asetukset
  sitten leviävät laitteiden välillä.

Tällä hetkellä en näe, että OsmAnd Pro tarjoaisi mitään merkittävää
lisäetua verrattuna OsmAnd+-sovellukseen omassa käytössäni. Itse
asiassa sovellus ilman eri laitteiden synkronointia on jopa parempi
toiminnallisuudeltaan, kun eri laitteet ei pääse sotkemaan toisiaan.

Bugiraportit olennaisilta osilta tuli tehtyä, joten katsotaan ensi
vuonna uudelleen, että millainen on OsmAnd Pron tilanne. Ainakin
selainkäyttöliittymä kaatuili edelleen, kun sitä tässä vähän aikaa
sitten testasin.

## Loppumietelmiä

Kaiken kaikkiaan vaiheittainen varustuksen ja menetelmien optimointi
mikrokartoitusta silmälläpitäen on mahdollistanut sen, että voi
keskittyä paikasta toiseen kulkemiseen ja datan päivittämiseen. Ilman,
että tarvitsee tapella työkalujen käyttäytymistä vastaan.

Joitain puutteita tässä varustuksessa vielä on verrattuna ideaaliin
kuviteltuun tilanteeseen. Paikannustarkkuuden puute jättää reilusti
toivomisen varaa, mutta tätä ei taideta helposti korjata alle 3000
euron sijoituksella, josta ammattitason GNSS-paikannuslaitteiden
hinnat näyttää alkavan. Lisäksi alueiden piirtäminen kentällä
mobiililaitteella on hyvinkin työlästä ja ei taida korjaantua ennen
kuin tähän löytyy tai tehdään ohjelmisto, jolla se onnistuu
mahdollisimman sujuvasti täppäritason käyttöliittymällä. Nyt joutuu
turvautumaan pistemuotoisiin paikkapisteisiin, kun merkkaa
pyöräparkkeja kartalle.
