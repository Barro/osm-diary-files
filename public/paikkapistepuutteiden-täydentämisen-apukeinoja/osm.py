#!/usr/bin/env python3

import math
import typing
import xml.etree.ElementTree as ET


def hav(theta: float) -> float:
    # https://en.wikipedia.org/wiki/Haversine_formula
    return (1 - math.cos(theta)) / 2


def distance_hav(lat1: float, lat2: float, lon1: float, lon2: float) -> float:
    # https://en.wikipedia.org/wiki/Haversine_formula
    lat1_r = math.radians(lat2)
    lat2_r = math.radians(lat2)
    lon1_r = math.radians(lon1)
    lon2_r = math.radians(lon2)
    earth_radius_m = 6371000
    sin = math.sin
    def sin2(x: float) -> float:
        t = sin(x)
        return t * t
    cos = math.cos
    h = sin2((lat2_r - lat1_r) / 2.0) + cos(lat1_r) * cos(lat2_r) * sin2((lon2_r - lon1_r) / 2.0)
    return 2 * earth_radius_m * math.asin(math.sqrt(h))


def type_osm(value: str) ->  typing.Dict[str, typing.Any]:
    tree = ET.parse(value)
    root = tree.getroot()
    features = {}
    nodes_way = {}
    for node in root.findall("node"):
        node_id = node.attrib["id"]
        node_lon = float(node.attrib["lon"])
        node_lat = float(node.attrib["lat"])
        tags = {
            "@id": f"node/{node_id}",
        }
        node_tags = node.findall("tag")
        nodes_way[node_id] = [node_lon, node_lat]
        if len(node_tags) == 0:
            continue
        for tag in node_tags:
            tags[tag.attrib["k"]] = tag.attrib["v"]
        feature_point = {
            "type": "Feature",
            "properties": tags,
            "geometry": {
                "type": "Point",
                "coordinates": [node_lon, node_lat],
            },
            "id": f"node/{node.attrib['id']}",
        }
        features[node_id] = feature_point

    for way in root.findall("way"):
        way_id = way.attrib["id"]
        tags = {
            "@id": f"way/{way_id}",
        }
        for tag in way.findall("tag"):
            tags[tag.attrib["k"]] = tag.attrib["v"]
        coordinates = []
        polygon_corners = way.findall("nd")
        for way_node in polygon_corners:
            way_node_id = way_node.attrib["ref"]
            coordinates.append(nodes_way[way_node_id])
        feature_polygon = {
            "type": "Feature",
            "properties": tags,
            "geometry": {
                "type": "Polygon",
                "coordinates": [coordinates],
            },
        }
        features[way_id] = feature_polygon

    out = {
        "type": "FeatureCollection",
        "timestamp": root.find("meta").attrib["osm_base"],
        "features": list(features.values()),
    }
    return out


def get_polygon_euclidic_center(coordinates: typing.List[typing.List[float]]) -> (float, float):
    lon_sum = 0.0
    lat_sum = 0.0
    points = 0
    assert len(coordinates) > 0
    for polygon in coordinates:
        assert len(polygon) > 0
        for point in polygon:
            points += 1
            lon, lat = point
            lon_sum += lon
            lat_sum += lat
    return lon_sum / points, lat_sum / points


def get_gpx_root() -> ET.Element:
    gpx = ET.Element(
        "gpx",
        {"xmlns:osmand": "https://osmand.net",
         "xmlns": "http://www.topografix.com/GPX/1/1",
         "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
         "xsi:schemaLocation": "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"
    })
    gpx.attrib["version"] = "1.1"
    e_metadata = ET.SubElement(gpx, "metadata")
    e_name = ET.SubElement(e_metadata, "name")
    e_name.text = "favorites"
    return gpx


def get_location(feature: typing.Dict[str, typing.Any]) -> typing.Tuple[float, float]:
    if feature["geometry"]["type"] == "Point":
        return feature["geometry"]["coordinates"][0], feature["geometry"]["coordinates"][1]
    elif feature["geometry"]["type"] == "Polygon":
        return get_polygon_euclidic_center(feature["geometry"]["coordinates"])
    else:
        assert False
    

def get_wpt(root: ET.Element, feature: typing.Dict[str, typing.Any]):
    lon, lat = get_location(feature)
    return ET.SubElement(root, "wpt", {"lon": str(lon), "lat": str(lat)})


def dump_gpx_root(root: ET.Element) -> str:
    return ET.tostring(root, "utf-8").decode("utf-8")
