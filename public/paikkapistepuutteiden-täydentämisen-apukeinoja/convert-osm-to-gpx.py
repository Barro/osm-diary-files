#!/usr/bin/env python3

import argparse
import json
import math
import os
import pathlib
import sys
import typing
import xml.etree.ElementTree as ET

import osm


def get_polygon_euclidic_center(coordinates: typing.List[typing.List[float]]) -> (float, float):
    lon_sum = 0.0
    lat_sum = 0.0
    points = 0
    assert len(coordinates) > 0
    for polygon in coordinates:
        assert len(polygon) > 0
        for point in polygon:
            points += 1
            lon, lat = point
            lon_sum += lon
            lat_sum += lat
    return lon_sum / points, lat_sum / points


def extend_gpx(
        root: ET.Element,
        feature: typing.Dict[str, typing.Any],
        wptype: str,
        color: str,
        icon: str,
        background: str,
) -> ET.Element:
    fixme_line = False
    if feature["geometry"]["type"] == "Point":
        wpt = ET.SubElement(
            root,
            "wpt",
            {"lon": str(feature["geometry"]["coordinates"][0]),
             "lat": str(feature["geometry"]["coordinates"][1])})
    elif feature["geometry"]["type"] == "Polygon":
        if len(feature["geometry"]["coordinates"][0]) == 2:
            fixme_line = True
        lon, lat = get_polygon_euclidic_center(feature["geometry"]["coordinates"])
        wpt = ET.SubElement(root, "wpt", {"lon": str(round(lon, 7)), "lat": str(round(lat, 7))})
    else:
        assert False

    name = ET.SubElement(wpt, "name")
    name.text = feature["properties"]["@id"]
    e_type = ET.SubElement(wpt, "type")
    e_type.text = wptype
    e_extensions = ET.SubElement(wpt, "extensions")
    e_color = ET.SubElement(e_extensions, "osmand:color")
    e_color.text = color
    return root


def dump_gpx_root(root: ET.Element) -> str:
    return ET.tostring(root, "utf-8").decode("utf-8")


def create_group(
        gpx: ET.Element,
        name: str,
        color: str,
        icon: str,
        background: str,
) -> ET.Element:
    e_extensions = ET.SubElement(gpx, "extensions")
    e_points_groups = ET.SubElement(e_extensions, "osmand:points_groups")
    e_group = ET.SubElement(
        e_points_groups,
        "group",
        {
            "name": name,
            "color": color,
            "icon": icon,
            "background": background,
        }
    )
    return gpx


def main(argv: typing.List[str]) -> int:
    ET.register_namespace('', "http://www.topografix.com/GPX/1/1")
    ET.register_namespace("osmand", "https://osmand.net")
    parser = argparse.ArgumentParser(prog=argv[0])
    parser.add_argument("osm", type=osm.type_osm)
    parser.add_argument("gpx", type=pathlib.Path)
    parser.add_argument("--wptype", required=True)
    parser.add_argument("--color", default="#ddcc2222")
    parser.add_argument("--icon", default="special_star")
    parser.add_argument("--background", default="circle")
    args = parser.parse_args(argv[1:])

    gpx = osm.get_gpx_root()
    
    for feature in args.osm["features"]:
        gpx = extend_gpx(gpx, feature, args.wptype, args.color, args.icon, args.background)
    gpx = create_group(gpx, args.wptype, args.color, args.icon, args.background)
    print(args.wptype, len(args.osm["features"]))

    with open(args.gpx, "wb") as fp:
        document = ET.ElementTree(gpx)
        ET.indent(document)
        document.write(
            fp,
            encoding="utf-8",
            xml_declaration=True,
            method="xml",
        )

    return os.EX_OK


if __name__ == "__main__":
    sys.exit(main(sys.argv))
