# Paikkapisteet kentältä kartalle

Lisäilen ja päivittelen OpenStreetMapiin
[pyörätelineitä](https://wiki.openstreetmap.org/wiki/Fi:Tag:amenity%3Dbicycle_parking)
menetelmin, jotka on muuttuneet ajan saatossa, kun olen löytänyt uusia
käytäntöjä ja sovelluksia tähän hommaan. OpenStreetMapin wikissä on
myös kattava yleisen tason esitys siitä, että [mitä kaikkia menetelmiä
on yleisellä tasolla kartoittamiseen
tarjolla](https://wiki.openstreetmap.org/wiki/Mapping_techniques),
josta voi myös hakea vinkkejä.

Seuraavissa luvuissa tarkastellaan hieman eri lähestymistapoja
pyöräpysäköintimahdollisuuksien syöttämiseksi OpenStreetMapiin, joita
olen itse käyttänyt saadakseni pyörätelinetiedon lisäämisen
OpenStreetMapiin suhteellisen vaivattomaksi. Näitä menetelmiä voi
hyödyntää myös minkä tahansa muun paikkapistetyypin lisäämisessä
OpenStreetMapiin.

## Varusteet

Liikun yleensä ympäriinsä sähköavusteisella polkupyörällä. Minulle on
tärkeämpää löytää ja päivittää pyöräilyyn liittyviä paikkapisteitä
kuin saada joltain tietyltä alueelta mahdollisimman kattavaa karttaa
aikaiseksi. Joten polkupyörä on tässä tapauksessa se loogisin
liikkumisväline.

Aluksi välineistössä oli pelkästään kännykkä, mutta sitten erikseen
hankin 8" täppärin pääasialliseksi OpenStreetMap-navigointi- ja
-lisäyslaitteeksi. Tämä kulkee pyörän tangossa
[RAM-kiinnikkeillä](https://rammount.com/) ([kuva 1](#pyörän-tangon-varustus)),
joka mahdollistaa koteloriippumattoman kiinnityksen. Tällä hoituu
suurin osa työsarasta, kun kentällä lisäilee ja tarkistaa
paikkapisteitä sitä mukaa, kun niitä tulee vastaan.

<figure id="pyörän-tangon-varustus">
<a href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/pyörän-tangon-varustus.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/pyörän-tangon-varustus.webp" height="400"
alt="Kuva pyörän tangosta, josta löytyy kaiutin, juoma ja täppäri"
title="Kuva pyörän tangosta, josta löytyy kaiutin, juoma ja täppäri"
/></a>
<figcaption>
Kuva 1: esimerkki kartoituksessa käytettävistä varusteista pyörän
tankoon kiinnitettynä.
</figcaption>
</figure>

Lisäksi kentällä ollessa kännykkä kulkee taskussa, jotta sen saa
nopeasti käyttöön siinä tapauksessa, että pitää kuvata jotain
jälkikäteistä lisäämistä varten. Kännykkä ei ole pyörän tangossa
kiinni sen takia, että huonommilla pinnoilla [kännykän kameralla on
tärinästä johtuva
hajoamisriski](https://www.ifixit.com/News/55673/iphone-smartphone-motorcycle-cameras-ois-focus-damage). En
kanna mukanani varavirtalähteitä tai muita pidemmillä
kartoitusreissulla hyödyllisiä varusteita, koska omat kartoitukseni
tapahtuu yleensä osana normaalia liikkumista tai parin tunnin
erillisinä reissuina hyvällä ja lämpimällä säällä.

Kotona isommanpuoleiset jälkikäteinen OpenStreetMapin muokkaaminen
hoituu usein koneella, jossa on vähintään 2 näyttöä käytössä ja
erillinen hiiri. Läppärin kosketuslevyä käyttäen OpenStreetMapin
muokkaus on suhteellisen tuskaista ja vaihtelu kuvien, videoiden ja
muokkaussovelluksen välillä vie turhan paljon aikaa.

## Paikkapisteiden lisääminen jälkikäteen

Työpöytäkäyttöön suunnitellut editorit [iD](https://ideditor.com/) ja
[JOSM](https://josm.openstreetmap.de/) muodostaa suurimman osan
OpenStreetMapin muokkauksista globaalisti ja näitä minäkin käytin
aluksi.

Käytännössä homma meni niin, että otin ensiksi videon kännykällä, tämä
kun aktivoi kännykän paikannusominaisuudet, ja sen jälkeen yhden
muutaman kuvan, jotta yksittäisiin kuviin rekisteröityisi
paikkatieto. Muutama ensimmäinen kerta meni pelkillä kuvilla, mutta se
osoittautui huonoksi ratkaisuksi sen suhteen, että pelkistä kuvista
katsomalla pyörätelineitä on hankala sijoittaa paikalleen
jälkikäteen.

<table id="kuvattu-ympäristö" style="width: 920px;">
<tr>
<td>
<figure>
<a href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/paikkatiedon-sisältävä-kuva.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/paikkatiedon-sisältävä-kuva.webp"
style="display: block; margin: 0 auto;"
height="294"
alt="Kuva pyörätelineistä paikkatiedon hyödyntämistä varten."
title="Kuva pyörätelineistä paikkatiedon hyödyntämistä varten."
/></a>
<figcaption>
Kuva 2: summittaisen paikkatiedon sisältävä kuva telineistä.
</figcaption>
</figure>
</td>
<td>
<figure>
<a href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/ominaisuudet-kuvaava-video.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/ominaisuudet-kuvaava-video.webp"
style="display: block; margin: 0 auto;"
height="294"
alt="Videoesimerkki pyörätelineistä ja niiden ympäristöstä"
title="Videoesimerkki pyörätelineistä ja niiden ympäristöstä"
/></a>
<figcaption>
Video 1: pyörätelineet ja näiden ympäristö kuvattuna videolle.
</figcaption>
</figure>
</td>
</tr>
</table>

Videossa (esimerkki <a href="#kuvattu-ympäristö">videossa
1</a>) tuli kuvattua ympäristöän ominaisuuksia sen verran, että
telineiden suhteellinen sijainti maamerkkeihin nähden on helpompi
määrittää myöhemmin. Lisäksi tuli puhuttua telineiden tai paikkojen
lukumäärä ja se tieto, että onko telineet katettuja vaiko
taivasalla.

Paikkatiedon sisältämä kuva (esimerkki <a
href="#kuvattu-ympäristö">kuvassa 2</a>) tai kuvat auttaa jälkikäteen
siinä, että nämä voi tuoda JOSMiin (<a href="#josm-paikkatieto">kuva
3</a>), joka osaa lukea kuvista paikkatiedon ja asettaa ne
kartalle. Telineiden tarkkaa sijaintia näistä kuvista ei yleensä ole
mahdollista saada tietoon, kun telineet yleensä sijaitsee [urbaaneissa
kanjoneissa](https://en.wikipedia.org/wiki/Urban_canyon). Mutta
summittainen sijainti liitettynä kuvattuun videoon auttaa virkistämään
muistia, että missä nämä tarkalleen oli.

<table id="josm-paikkatieto">
<tr>
<td>
<a href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/josm-paikkatietoisa-kuva.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/josm-paikkatietoisa-kuva.webp"
style="display: block; margin: 0 auto;"
height="440"
alt="JOSMiin tuotu paikkatiedon sisältävä kuva pyörätelineistä"
title="JOSMiin tuotu paikkatiedon sisältävä kuva pyörätelineistä"
/></a>
</td>
</tr>
<tr>
<td>
Kuva 3: JOSMiin tuotu paikkatiedon sisältävä kuva pyörätelineistä.
</td>
</tr>
</table>

Varsinaisen pyörätelineiden kuvaamisen ja OpenStreetMapiin lisäämisen
välillä voi mennä helposti useampi viikkokin riippuen siitä, että
paljonko materiaalia tuli hankittua ja mikä on oma OpenStreetMapin
päivitysjaksaminen. Jälkikäteinen pyöräpysäköinnin lisääminen
nimittäin vie joskus saman verran aikaa kuin pidempi
materiaalinhankintareissu, sisältäen siirtymät.

## Paikkapisteiden lisääminen kentällä

Suurin osa pyöräpysäköintialueista sisältää sen verran vähän
pyörätelineitä, että ne voi hyvällä omatunnolla lisätä yhtenä pisteenä
kartalle. Tässä tapauksessa usein kokonaisuutena nopein vaihtoehto on
jälkikäteisen lisäämisen sijaan lisätä paikkapiste suoraan kentällä
OpenStreetMapiin. Sillä edellytyksellä, että varsinainen lisääminen
kentällä olevalla laitteella ei juurikaan vie aikaa.

Kentällä ollessa tulee koettua samat ongelmat paikkatiedon suhteen
mitä jälkikäteisessä paikkapisteiden lisäämisessä. Pyörätelineet ja
muut kiinnostavat paikkapisteet yleensä sijaitsee urbaaneissa
kanjoneissa, jolloin käytetyn laitteen kuvittelema paikka heittää
helposti kymmenillä metreillä ideaaliolosuhteiden muutaman metrin
sijaan. Ellei ole valmis käyttämään tuhansia euroja [ammattilaistason
laitteisiin](https://developers.arcgis.com/appstudio/guide/prepare-for-high-accuracy-data-collection/)
ja mahdollisesti aikaavieviä mittausmenetelmiä.

Realistisena käytettävänä laitteena kentällä on joko kännykkä tai
täppäri, joissa datan syöttäminen on suhteellisen hidasta ja
kömpelöä. Joten tavanomaisessa paikkapisteiden lisäämisessä
syötettävän tietomäärän tulisi olla mahdollisimman vähän toimintoja
vaativaa. Tätä tuskaa helpottamaan eri sovelluksissa on eri
lähestymistapoja ja rajoitteita, joita tarkastellaan
sovelluskohtaisesti seuraavaksi.

### Vespucci

[Vespucci](https://vespucci.io/) on suhteellisen monipuolinen
OpenStreetMap-editori mobiililaitteille, jolla voi luoda pistemäisten
paikkapisteiden lisäksi myös alueita. Valitettavasti sen
käyttöliittymä on turhan raskas nopeaan paikkapisteiden lisäämiseen ja
muokkaamiseen kentällä verrattuna vaihtoehtoihin. En tarkastele
Vespuccin ominaisuuksia tätä enempää, sillä laajemmat alueina
käsiteltävät pyörätelinekokonaisuudet hoituu helpommin kuvaamalla alue
ja lisäämällä tiedot jälkikäteen työpöytäsovelluksella.

### OsmAnd

[OsmAnd](https://osmand.net/) tarjoaa päätoimintoinaan
verkkoyhteydettömät kartat ja
navigaatiomahdollisuuden. OpenStreetMapin muokkaustoiminnallisuus ei
ole oletuksena päällä, mutta tarjoaa suhteellisen joustavan
käyttöliitymän pistemuotoisten paikkapisteiden lisäämiseen ja
muokkaamiseen.

Omassa käytössäni OsmAndin [Quick Action](https://osmand.net/docs/user/widgets/quick-action/)
-toiminnallisuus on osoittautunut parhaaksi lähestymistavaksi. Quick
Actioneilla saa halutunlaisten paikkapisteiden lisäämisessä ja
tarkastelussa tarvittavan yleisimmät toiminnot tehtyä parilla
täpäytyksellä. Tämä mahdollistaa sen, että uusien pyörätelineiden
lisääminen vie noin suunnilleen saman verran aikaa kuin ympäristön
kuvaaminen veisi jälkikäteistä lisäämistä varten. Usein jopa vähemmän
aikaa. Yleensä sijainnin lisäksi valmiiksi täytettyyn tunnisterunkoon
pitää vain kirjata telineistä johdettava pysäköitävien pyörien
lukumäärä.

<table id="osmand-quick-action">
<tr style="vertical-align: top;">
<td>
<figure>
<a href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/osmand-quick-actions-kohdistettu-sijaintiin.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/osmand-quick-actions-kohdistettu-sijaintiin.webp"
style="display: block; margin: 0 auto;"
height="400"
alt="OsmAndin Quick Action -näkymä kohdistettuna nykyiseen sijaintiin."
title="OsmAndin Quick Action -näkymä kohdistettuna nykyiseen sijaintiin"
/></a>
<figcaption>
Kuva 4: OsmAndin Quick Action -näkymä kohdistettuna nykyiseen
sijaintiin. Lisäksi tässä on erikseen korostettu paikkapistetyyppi,
jotka ei välttämättä piirry käytetyllä kartan näkymällä.
</figcaption>
</figure>
</td><td>
<figure>
<a href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/osmand-quick-actions-kohdistettu-viereen.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/osmand-quick-actions-kohdistettu-viereen.webp"
style="display: block; margin: 0 auto;"
height="400"
alt="OsmAndin Quick Action -näkymä sijainti korjattuna ja kartta hieman loitonnettuna"
title="OsmAndin Quick Action -näkymä sijainti korjattuna ja kartta hieman loitonnettuna"
/></a>
<figcaption>
Kuva 5: OsmAndin Quick Action -näkymä sijainti korjattuna ja kartta hieman loitonnettuna.
</figcaption>
</figure>
</td><td>
<figure>
<a href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/osmand-bicycle-parking-capacity-syöttö.webp"><img src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/osmand-bicycle-parking-capacity-syöttö.webp"
style="display: block; margin: 0 auto;"
height="400"
alt="OsmAndin tietojensyöttöruutu, jossa kysytään pysäköitävien pyörien määrää (capacity-tunniste)"
title="OsmAndin tietojensyöttöruutu, jossa kysytään pysäköitävien pyörien määrää (capacity-tunniste)"
/></a>
<figcaption>
Kuva 6:OsmAndin tietojensyöttöruutu, jossa kysytään pysäköitävien pyörien määrää.
</figcaption>
</figure>
</td><td>
<figure>
<a href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/osmand-bicycle-parking-valmiit-kentät.webp"><img src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/osmand-bicycle-parking-valmiit-kentät.webp"
style="display: block; margin: 0 auto;"
height="400"
alt="OsmAndin tietojensyöttöruudussa valmiiksi syötetyt harvemmin vaihtelevat tunnisteet (bicycle_parking, covered, access, fee)"
title="OsmAndin tietojensyöttöruudussa valmiiksi syötetyt harvemmin vaihtelevat tunnisteet (bicycle_parking, covered, access, fee)"
/></a>
<figcaption>
Kuva 7: OsmAndin tietojensyöttöruudussa valmiiksi syötetyt harvemmin vaihtelevat tunnisteet.
</figcaption>
</figure>
</td>
</tr>
</table>

[Kuvista 4-7](#osmand-quick-action) näkee sen, että seuraavat vaiheet
OsmAndin Quick Action -nappulan painamisen jälkeen tarvitaan uuden
paikkapisteen lisäämiseksi:

* Kuva 4: lisättävän paikkapisteen mahdollinen sijainti on
  kohdistettuna nykyiseen sijaintiin. Tätä yleensä pitää hieman
  korjata ja OsmAndin mielivaltaiseen tarkkuuteen pääsevä näkymä
  tarjoaa tähän hyvät työkalut. Lisäksi tässä on erikseen tuotu
  näkyviin oranssilla paikkapistetyyppi, jollainen ei välttämättä
  ensisijaisesti piirry käytetyllä kartan loitonnustasolla
  OsmAndissa.
* Kuva 5: lisättävän paikkapisteen sijaintia on korjattu ja
  karttanäkymään mittakaavaa muutettu sopivaksi.
* Kuva 6: uuden paikkapisteen lisäämistä varten on valmis runko, josta
  puuttuu pelkästään tieto siitä, että montako pyörää pystyy
  pysäköimään tähän pyöräpysäköinti-tyyppiseen
  paikkapisteeseen. Jos yhtään tunnistetta ei jätä tyhjäksi, niin
  OsmAnd ei edes näytä tätä ruutua. Mutta pysäköitävien pyörien määrä
  vaihtelee sen verran usein, että tämän syöttäminen joka kerta on
  hyvä idea. Muita tunnisteita voi myös korjata tässä samalla, jos ne
  poikkeaa oletuksista.
* Kuva 7: harvemmin muuttuvat tunnisteet lisättävässä
  pyöräpysäköinnissä. Näitä iD-editori kyselee oletuksena ja monia
  näistä käytetään eri kartoissa ja karttakerroksissa kertomaan hieman
  pyöräpysäköinnin ominaisuuksista.

Jos näillä tunnisteiden oletusarvoilla pärjää, niin uuden
paikkapisteen syöttäminen OsmAndilla on tosiaan todella nopeaa, joka
vaatii alle 10 painallusta. Jos taas tietoja pitää usein korjata,
niin OsmAnd ei ole kaikista kätevin tähän, sillä kyllä-/ei-tyyppiset
arvotkin pitää erikseen kirjoittaa näihin kenttiin. Mutta omassa
käytössäni tämä on osoittautunut nopeimmaksi ja vaivattomimmaksi
tavaksi syöttää uusia paikkapisteitä.

### Every Door

[Every Door](https://every-door.app/) on mobiilisovellus, jonka
käyttöliittymä on erikseen optimoitu paikkapisteiden muokkaamista ja
lisäämistä silmälläpitäen. Every Door tarjoaa eri näkymiä
paikkapisteiden tarkastelua varten ([kuvat 8 ja 9](#every-door)) ja
tarjoaa etukäteen määriteltyjä valintoja uuden paikkapisteen
lisäämiseksi, kuten yleisimmin käytetyt tunnisteet paikkapisteelle
([kuva 10](#every-door)). Tämä tarjoaa kätevän käyttöliittymän siinä
tapauksessa, että aktiivisesti lisäilee ja muokkaa monentyyppisiä
paikkapisteitä kännykällä.

<table id="every-door">
<tr style="vertical-align: top;">
<td>
<figure>
<a
href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/every-door-paikkapisteet-maasto.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/every-door-paikkapisteet-maasto.webp"
style="display: block; margin: 0 auto;"
height="400" /></a>
<figcaption>
Kuva 8: Every Doorin paikkapistenäkymä, jossa on suodatettu maastoon
liittyvät paikkapisteet näkyviin.
</figcaption>
</figure>
</td><td>
<figure>
<a
href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/every-door-paikkapisteet-palvelut.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/every-door-paikkapisteet-palvelut.webp"
style="display: block; margin: 0 auto;"
height="400" /></a>
<figcaption>
Kuva 9: Every Doorin paikkapistenäkymä, jossa on suodatettu
palveluihin liittyvät paikkapisteet näkyviin.
</figcaption>
</figure>
</td><td>
<figure>
<a
href="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/every-door-bicycle-parking-data.webp"><img
src="https://barro.gitlab.io/osm-diary-files/paikkapisteet-kentältä-kartalle/every-door-bicycle-parking-data.webp"
style="display: block; margin: 0 auto;"
height="400" /></a>
<figcaption>
Kuva 10: pyöräpysäköintityyppisen paikkapisteen datansyöttöruutu.
</figcaption>
</figure>
</td>
</tr>
</table>

Omassa käytössäni Every Door ei ole osoittautunut yhtä käteväksi kuin
OsmAnd. Every Doorissa olemassaolevat paikkapisteet jakautuu kahteen
eri näkymään, joiden ennaltamäärättyjä valintoja ei voi
vaihtaa. Lisäksi uusien rajatuntyyppisten paikkapisteiden syöttäminen
ei ole yhtä kätevää kuin OsmAndissa. Rasteripohjainen karttanäkymä ei
myöskään ole yhtä mukava katsoa kuin OsmAndin suhteellisen
mielivaltaiseen tarkkuuteen yltävä vektoripohjainen kartta. Every
Doorista puuttuu myös kaikenlaisia muita pieniä ominaisuuksia, joita
OsmAndista löytyy. Ottaen kuitenkin huomioon että tämä on suhteellisen
uusi yhden henkilön projekti, tulevaisuudessa tämä voi kehittyä
melkoisen käteväksi sovellukseksi OpenStreetMapin muokkaamiseen.

### StreetComplete

[StreetComplete](https://streetcomplete.app/) on OpenStreetMapin
muokkaamisen pelillistävä sovellus, joka on tarkoitettu täydentämään
olemassaolevien paikkapisteiden tietoja. StreetCompletessa puuttuvia
tietoja kysytään yksi kerrallaan ja vastausvaihtoehtoja usein
tarjotaan rajattu määrä. Pyörätelinetyyppisten paikkapisteiden osalta
StreetCompletella voi täydentää seuraavia ominaisuuksia:

* [Pyörätelineiden tyyppi](https://wiki.openstreetmap.org/wiki/Key:bicycle_parking)
  valittavissa rajatusta telinevalikoimasta.
* Ovatko pyörätelineet
  [katettuja](https://wiki.openstreetmap.org/wiki/Key:covered).
* [Kuinka monta pyörää pyörätelineisiin mahtuu](https://wiki.openstreetmap.org/wiki/Key:capacity).

StreetComplete tarjoaa nopean tavan täydentää näitä tietoja, sillä siinä
paikkapisteen valinta on suhteellisen mutkatonta ja heti pääsee
täydentämään kysyttyä ominaisuutta. StreetCompleten lähestymistapa
tietojen täydentämiseen on suhteellisen rajoittunut, mutta ajaa
asiansa olemassaolevien pyöräpysäköintipaikkapisteiden suhteen.

## Loppupäätelmiä

Jos erikoistuu hyvin rajattunlaiseen paikkapistevalikoimaan, kuten
minä teen, niin kentällä on hyvinkin nopeasti ja vaivattomasti
mahdollista lisätä paikkapisteitä OpenStreetMapiin. Tähän
käyttökelpoisin sovellus omassa käytössäni OsmAnd+ (maksullinen
versio OsmAndista), joka tarjoaa pikavalinnat uusien paikkapisteiden
lisäämiseen. Joissain erikoistapauksissa JOSMin tai iD:n ominaisuuksia
tarvitaan, mutta suurin osa pyörien pysäköintiin liittyvien
paikkapisteiden lisäyksistä ja muokkauksista hoituu näihin
turvautumatta.

Olemassaolevien paikkapisteiden muokkaus myös sujuu OsmAndilla
joustavasti, vaikkakin hieman vaivalloisesti. Lisäksi OsmAnd tarjoaa
[reittien](https://osmand.net/docs/user/personal/tracks/) avulla saa
tehtyä merkintöjä siitä, että mitkä olemassaolevat paikkapisteet tai
paikat vaativat lähempää tarkastelua. Tämä kuitenkin vaatisi toisen
artikkelin.
