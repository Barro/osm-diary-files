# Päiväkotien ja koulujen pyöräpysäköintitilanne

Kävin tässä loppukesästä ja alkusyksystä läpi merkittävän määrän
Espoon ja Helsingin päiväkodeista, kouluista ja muita koulutusta
tarjoavia paikkoja, jotka löytyi OpenStreetMapin datan
perusteella. Tähän myös kuului pieni osa etelä-Vantaata, [kuvan
1](#kartta-alue) rajaaman alueen sisältä.

<figure id="kartta-alue">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/kartta-alue.html"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/kartta-alue.webp"
alt="Päiväkotien ja koulujen pyöräpysäköintitilanteen kartoitusalue"
title="Päiväkotien ja koulujen pyöräpysäköintitilanteen kartoitusalue"
width="880"
height="558"
/></a>
<figcaption>
<a href="#kartta-alue">Kuva 1</a>: Päiväkotien ja koulujen
pyöräpysäköintitilanteen kartoitusalue.
</figcaption>
</figure>

Inspiraatio tähän tuli seuraavasta
[Facebook-ketjusta](https://www.facebook.com/groups/1016789188364911/posts/8173728946004197/),
jossa tuskailtiin Espoon päiväkotien tilannetta pyörällä liikkuvien
pysäköintimahdollisuuksien suhteen. Erityisesti, kun pääkaupunkiseudun
kaupungeilla on suuria juhlapuheita kestävän liikkumisen suhteen,
mutta todellisuus rahankäytön ja asenteen osalta käytännössä jättää
kestävän liikkumisen juhlapuheisiin. 

Pääkaupunkiseudun liikennejärjestelmä nojaa voimakkaasti
henkilöautoiluun ja joukkoliikenteeseen. Helsingin seudulla, sisältäen
myös kehyskuntia, [käytetään vuosittain
luokkaa 400 miljoonaa
euroa](https://www.mustread.fi/artikkelit/hyotyja-maksaa-sanotaan-helsingin-seudun-liikenneinvestoinneista-mutta-kuka-lopulta-hyotyy-toimivasta-liikennejarjestelmasta/). Tämä
on edelleen yli kymmenkertaisesti mitä pyöräilyyn ja kävelyyn on
osoitettu viime vuosina. Sen seurauksena pienet yksityiskohdat, jotka
tekevät kävelystä ja pyöräilystä varteenotettavamman vaihtoehdon
liikkumiseen, tuppaa unohtumaan.

[Pyöräpysäköinnin laatu](https://www.jyps.fi/pyoraily-liikennemuotona-ja-harrastuksena/hyvat-pyorailyolosuhteet/hyvat-pyoraparkit)
tai
[laaduttomuus](https://www.kaupunkifillari.fi/2020/12/01/heikoin-lenkki-pitaa-olla-oma-lukko/)
on yksi näistä yksityiskohdista, jotka vaikuttaa siihen, että miten
paljon joutuu stressaamaan, kun liikkuu pyörällä paikasta
toiseen. On pyörän peräkärryjä, kapearenkaisia pyöriä, leveärenkaisia
pyöriä, rahtipyöriä, levyjarrulla varustettuja pyöriä ja useampi kuin
kaksirenkaisia pyöriä, joita tyypillinen [kuvasta
2](#pyöräteline-kiekonväännin) Suomesta löytyvä
kiekonväännintyyppinen pyöräteline ei palvele. Erityisesti, jos haluaa
pyörän rungosta kiinni pidemmäksi aikaa johonkin kiinteästi
asennettuun tai hankalasti liikuteltavaan asiaan.

<figure id="pyöräteline-kiekonväännin">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/päiväkoti-pyöräpysäköinti-tyypillinen-2.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/päiväkoti-pyöräpysäköinti-tyypillinen-2.webp"
alt="Kolmipaikkainen kiekonväännintyyppinen pyöräteline talon edessä."
title="Kolmipaikkainen kiekonväännintyyppinen pyöräteline talon edessä."
width="880"
height="418"
/></a>
<figcaption>
<a href="#pyöräteline-kiekonväännin">Kuva 2</a>: Tyypillinen Suomessa
käytetty kiekonväännintyyppinen pyöräteline.
</figcaption>
</figure>

Tässä artikkelissa väännetään rautalangasta asioita, jotka voi tuntua
itsestäänselvyyksiltä. Mutta kun on lähtenyt erikseen tutkimaan
pyöräliikenteen toteutuksen ongelmakohtia, niin havaitsee sen, että
nämä eivät ole itsestäänselvyyksiä.

# Menetelmiä

Ideana minulla oli tässä käydä läpi päiväkotien ja koulujen
pyöräpysäköintiratkaisuja hiljalleen, samalla kun liikun ympäri
pääkaupunkiseutua. Tämän kartoitusprojektin kirjanpito tuli hoidettua
kutakuinkin seuraavanlaisesti:

1. Haetaan [Overpass-kyselyillä](https://overpass-turbo.eu/) tieto
   kaikista sopivista kohteista tälle kartoitusprojektille.
2. Vaivutaan epätoivoon hakujen palauttamien pistemäärien seurauksena
   ja todetaan projektin jatkuvan todennäköisesti pitkälle ensi
   vuoteen.
3. Lisätään haetut paikkapisteet ja alueiden keskipisteet
   [OsmAndin My Placesin Favorites](https://osmand.net/docs/user/personal/myplaces/)-osioon.
4. Poistetaan pisteitä suosikeista sitä mukaa mitä näitä tulee käytyä
   läpi ja viedään suosikit varmuuden vuoksi Google Driveen päivän
   päätteeksi.

Eli sen sijaan, että kävisin paikkoja läpi alueittain ja merkkaisin
käydyt alueet tehdyiksi, pystyin käymään läpi saman alueen paikkoja
useammalla läpikululla. Tämä mahdollisti sen, että pystyin yhdistämään
näiden paikkojen läpikäymistä osaksi normaalia liikkumista. Tällöin ei
tarvinnut käyttää niin paljon aikaa siihen, että kulkee kotoa erikseen
jollekin tietylle alueelle pelkästään tätä kartoitusprojektia varten.

# Pyöräpysäköinnin suunnitteluohje

Helsingin kaupungilla on ollut [pyöräpysäköinnin
suunnitteluohje](https://www.hel.fi/hel2/ksv/julkaisut/los_2016-1.pdf)
vuodesta 2016 alkaen. Suunnitteluohjeen sivulla 12 erikseen otetaan
kantaa päiväkotien, koulujen ja muiden oppilaitosten pyöräpysäköinnin
vaatimuksiin eri käyttäjäryhmien suhteen.

Tässä on ollut 8 vuotta aikaa saattaa polkupyörien pysäköintiratkaisut
näihin suunnitteluohjeisiin sopivammiksi. Rakennuskannalla tietenkin
menee enemmän aikaa kuin 8 vuotta uudistua, mutta yksityiskohtia voi
silti parantaa kokonaisuutta muuttamatta.

# Päiväkodit

Päiväkodit on sikäli erikoisessa asemassa pyöräliikenteen suhteen,
että suurin osa päiväkodeissa päivänsä viettävistä ei kulje sinne
itsenäisesti. Sanon suurin osa, sillä esimerkiksi päiväkodin
henkilökunta varmasti kulkee ilman saattamista. Lapset eivät
säännönmukaisesti yksin kulje päiväkoteihin tai sieltä kotiin. Tämä
tarkoittaa sen, että vanhempien täytyy tuoda lapsi päiväkotiin jollain
keinolla. Nuorimpien osalta, jotka eivät pysty juurikaan kävellen
liikkumaan, se tarkoittaa jotain kuljetusvälinettä.

## Haasteet kuljetukselle

Kun lapset tuodaan päiväkotiin, usein heidän mukanaan kulkee
lastenvaunut tai joku muu kuljetusväline, kuten polkupyörän
peräkärry. Hieman vanhemmilla päiväkoti-ikäisillä lapsilla saattaa olla
käytössään potkupyörä tai jopa pieni polkupyörä. Ei ole mitenkään
harvinaista, että päiväkotiin kuljetettavan lapsen lisäksi mukana
kulkee tavaraa, jota tarvitaan myös silloin, kun lapsi haetaan
päiväkodista.

Tavaroiden päivänaikaista säilömistä ei välttämättä ajattele, jos on
tottunut kuljettamaan lapsiaan autolla ympäriinsä. Auto itsessään
tarjoaa tilan säilyttää lapseen liittyviä tavaroita. Tämä kätevyys
tavaran säilytyksen suhteen tarjoaa yhden ison kannusteen käyttää
autoa matkoihin, vaikka sillä ei muuntyyppistä merkittävää etua
välttämättä saisikaan.

Osa päiväkodeista on ratkaissut kävellen, polkupyörällä ja
joukkoliikenteellä liikkuvien ongelman tarjoamalla erillisen
suunnitteluohjeiden mukaisen katetun tilan lastenvaunuille ja muille
liikkumiseen liittyville tarvikkeille. Tämä ei ole kuitenkaan
itsestään selvää, eikä ole aina mahdollistakaan päiväkodeissa, jotka
on perustettu kerrostalojen kivijalkaan. Mutta jopa viime vuosina
rakennetuissa päiväkodeissa laadukas ja riittävä
pyöräpysäköintiratkaisu ei tunnu olevan itsestäänselvyys.

## Käytettyjä heikkolaatuisia pyöräpysäköintiratkaisuja

Yleisin ratkaisu pyöräpysäköinnille on ollut asettaa päiväkodin
ulko-oven tai pihalle menevän portin eteen yksi tai useampia
[kuvan 3](#päiväkodit-tyypillinen-pyöräteline) mukaisia johdannossa
mainittuja kiekonväännintyyppisiä pyörätelineitä. Paikkamäärältään
nämä usein ovat riittämättömiä lyhyen käynnin ajaksi, kun näihin
samoihin telineisiin odotetaan jätettävän myös päivän ajaksi lasten ja
henkilökunnan pyörät.

<figure id="päiväkodit-tyypillinen-pyöräteline">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/päiväkoti-pyöräpysäköinti-tyypillinen-1.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/päiväkoti-pyöräpysäköinti-tyypillinen-1.webp"
alt="Päiväkodin portin vierestä löytyvä viisipaikkainen kiekonväänninteline."
title="Päiväkodin portin vierestä löytyvä viisipaikkainen kiekonväänninteline."
width="880"
height="647"
/></a>
<figcaption>
<a href="#päiväkodit-tyypillinen-pyöräteline">Kuva 3</a>: Tyypillinen
ratkaisu päiväkotien pyöräpysäköintivaatimuksen
toteuttamiseksi. Heikkolaatuinen taivasalla oleva teline, jossa on
liian vähän paikkoja.
</figcaption>
</figure>

Näitä [kuvan 3](#päiväkodit-tyypillinen-pyöräteline) mukaisia
pyörätelineitä en omilla kartoitusretkilläni edes kirjaa
kartalle. Näiden kirjaaminen tuntuisi ajantuhlaukselta sen osalta,
mitä OpenStreetMapin päivityksilläni haen. En usko, että päiväkodin
henkilökuntakaan näitä juurikaan käyttää polkupyöriensä
säilyttämiseen. Erityisesti, kun nämä telineet ovat usein myös
ainoita havaittavia telineitä päiväkodin alueella.

<figure id="päiväkoti-pyörät-lastenvaunut-katettu">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/päiväkoti-pyörät-lastenvaunut-katettu.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/päiväkoti-pyörät-lastenvaunut-katettu.webp"
alt="10 paikkainen kiekonväänninteline ja vieressä lastenvaunut."
title="10 paikkainen kiekonväänninteline ja vieressä lastenvaunut."
width="880"
height="490"
/></a>
<figcaption>
<a href="#päiväkoti-pyörät-lastenvaunut-katettu">Kuva 4</a>: Yhden
autopaikan vaatiman alueen kokoinen katettu tila polkupyörille ulkona
päiväkodin portin vieressä.
</figcaption>
</figure>

[Kuvassa 4](#päiväkoti-pyörät-lastenvaunut-katettu) näkyy sama
tyypillinen telinetyyppi, mutta tässä tapauksessa tila on
katettu. Tämä mahdollistaa sen, että jos polku- ja potkupyörät
jätetään vähän sateisemmaksi tai lumisemmaksi päiväksi, niin pyörän
liikkeellesaamisen suhteen ei ole ongelmia. En tiedä, että mikä
käytäntö tässä päiväkodissa on lastenvaunujen suhteen, että onko tämä
tila ainoa paikka jättää vaunut päiväksi, vai onko niille osoitettu
jokin toinen tila.

<figure id="päiväkoti-henkilöautopysäköinti">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/päiväkoti-henkilöautopysäköinti.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/päiväkoti-henkilöautopysäköinti.webp"
alt="Parkkialue, jonka läpi pääsee päiväkodin portille."
title="Parkkialue, jonka läpi pääsee päiväkodin portille."
width="880"
height="205"
/></a>
<figcaption>
<a href="#päiväkoti-henkilöautopysäköinti">Kuva 5</a>: 12 auton
parkkipaikat päiväkodin portin edessä.
</figcaption>
</figure>

Vertailukohtana kuitenkin se, että mikä ratkaisu on tehty
autoille tilankäytön suhteen tässä samassa päiväkodissa. [Kuvan
4](#päiväkoti-pyörät-lastenvaunut-katettu) alue polkupyörille vie noin
kahden autopaikan vaatiman tilan verran tilaa ja siinä on paikat 10
pyörälle ja puolet tyhjää johonkin muuhun käyttöön. Autoille on taas
[kuvan 5](#päiväkoti-henkilöautopysäköinti) mukaisesti varattu 12
paikkaa, jotka ovat vieläpä siinä reitillä, mistä on loogisinta kulkea
pyörällä päiväkodin portille.

Parkkipaikan vierestä pääsee tässä tapauksessa pyörällä kulkemaan,
mutta päiväkodin alueelle on silti vain yksi yhteinen reitti autoille
ja pyörille. Parkkipaikan kiertämällä joutuu kuitenkin jakamaan
kulkuväylän päiväkodin ovelle kulkevien kanssa.

## Parempilaatuiset päiväkotien pyöräpysäköintiratkaisut

Vaikka suurimmassa osassa päiväkodeista pyöräpysäköintiratkaisut on
heikkoja, niin parempiakin ratkaisuja on. Osa niistä on vieläpä
suhteellisen helposti järjestettävissä, jos tahtoa (eli budjettia)
riittää.

### Siirrettävät runkolukittavat telineet

Siirrettävät runkolukituksen mahdollistavat telineet on askel
eteenpäin pyöräpysäköinnin järjestämisen suhteen.

<figure id="lehdokin-päiväkoti-runkolukittava-teline">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/lehdokin-päiväkoti.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/lehdokin-päiväkoti.webp"
alt="12-paikkainen runkolukittava siirrettävä teline Lehdokin päiväkodin sisäänkäynnin vieressä."
title="12-paikkainen runkolukittava siirrettävä teline Lehdokin päiväkodin sisäänkäynnin vieressä."
width="880"
height="459"
/></a>
<figcaption>
<a href="#lehdokin-päiväkoti-runkolukittava-teline">Kuva 6</a>:
Järeämpi suhteellisen kapeilla väleillä varustettu, mutta kuitenkin
siirrettävä runkolukituksen mahdollistava teline Lehdokin päiväkodin
pihassa sisäänkäynnin vieressä.
</figcaption>
</figure>

[Kuvan 6](#lehdokin-päiväkoti-runkolukittava-teline) tyyliset
yhdeksi kokonaisuudeksi hitsatut telineet olisivat helpoimpia
ratkaisuja päiväkotien pyöräpysäköinnin parantamiseksi. Näitä saa eri
kokoisina ja todennäköisesti myös eri materiaalivahvuuksisina. Näitä
vasten saa runkolukituksella jätettyä pyörän pystyyn ja nämä eivät
juurikaan rajoita käytettävää renkaan kokoa tai leveyttä. Eivät
myöskään väännä vanteita tai levyjarruja. Näitä telineitä ei myöskään
saa purettua palasiksi pelkkää ruuvin- tai mutterinväännintä
käyttäen.

Kun tällaisessta telineessä on riittävästi pyöriä kiinni, niin näitä
ei kahdestaan myöskään saa helposti liikuteltua. Tämä telinetyyppi on
periaatteessa vielä täytettävissä jollain materiaalilla, jolla teline
on mahdollista saada pysymään vielä paremmin aloillaan. Eivät nämä
yhdestä osasta koostuvat siirrettävät telineet täydellisiä ole, ja
näiden hankinnoissa voi mokata monella eri tapaa, mutta parempia kuin
mitä yleensä näkee.

### Maahan upotetut runkolukittavat telineet

Luultavasti paras yhdistelmä tilankäytön, jämäkkyyden ja
käyttökelpoisuuden suhteen on kiinteästi maahan upotetut
runkolukittavat pyörätelineet. Mutta nämä yleensä vaativat betonivalun
tai muun suuremman remontin, jotta niitä saa asennettua.

<figure id="verkkosaaren-päiväkoti-jättöliikenne">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/verkkosaaren-päiväkoti-jättöliikenne.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/verkkosaaren-päiväkoti-jättöliikenne.webp"
alt="6 kaaritelinettä, jotka mahdollistavat polkupyörien runkolukituksen."
title="6 kaaritelinettä, jotka mahdollistavat polkupyörien runkolukituksen."
width="880"
height="397"
/></a>
<figcaption>
<a href="#verkkosaaren-päiväkoti-jättöliikenne">Kuva 7</a>:
Runkolukituksen mahdollistavilla kaaritelineillä varustettu
pyöräpysäköinti Verkkosaaren päiväkodin portin vieressä.
</figcaption>
</figure>

Maahan upotetuista runkolukittavista telineistä löytyy esimerkki
Verkkosaaren päiväkodista [kuvassa
7](#verkkosaaren-päiväkoti-jättöliikenne). Tässä on suunnittelu- ja
rakennusvaiheessa selkeästi pohdittu pyöräpysäköinnin vaatimuksia
jättöliikenteelle. Ei ihan kuitenkaan täysin onnistuttu, sillä
päästäkseen tuonne, pitää pyörä kuljettaa joko autojen jättöliikenteen
kadunvarsiparkin vieressä olevan kanttikiven yli, tai jalkakäytävällä.

<figure id="verkkosaaren-päiväkoti-katettu">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/verkkosaaren-päiväkoti-katettu.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/verkkosaaren-päiväkoti-katettu.webp"
alt="Runkolukittavia kaaritelineitä sisältävä katettu alue, jossa
yhdessä telineessä on pieni lasten polkupyörä."
title="Runkolukittavia kaaritelineitä sisältävä katettu alue, jossa
yhdessä telineessä on pieni lasten polkupyörä."
width="880"
height="530"
/></a>
<figcaption>
<a href="#verkkosaaren-päiväkoti-katettu">Kuva 8</a>: Verkkosaaren
päiväkodin katettu pyöräpysäköintialue. Kaaritelineet mahdollistaa
monipuoliset lukitsemisvaihtoehdot.
</figcaption>
</figure>

Verkkosaaren päiväkodissa on myös toinen pyöräpysäköintialue, joka
näkyy katettuna [kuvassa 8](#verkkosaaren-päiväkoti-katettu) ja
mahdollistaa jossain määrin sääsuojatun paikan jättää pyörän
päiväksi. Tämä kuva näyttää sen, että vaikka pyörä olisi pieni, niin 
kaariteline silti mahdollistaa sen pysäköinnin. Vaikka kaaritelineen
vaakaputkeen ei ylettäisi, niin pystyputki on silti käyttökelpoinen ja
mahdollistaa lukituksen laajalle skaalalle erilaisia pyöriä.

### Telineet laatikkopyörille

[Laatikkorakenteella varustetut
taakkapyörät](https://www.youtube.com/watch?v=rQhzEnWCgHA), eli
tässä laatikkopyörät, ovat pyörätyyppi, joka mahdollistaa tavaran ja
lasten kuskaamisen ilman suurempia kikkailuvalintoja. Laatikkopyörät
vaativat enemmän tilaa ja ovat rakenteeltaan erilaisia verrattuna
tyypilliseen polkupyörään, jonka runko on suhteellisen lyhyt ja
renkaat suhteellisen isoja. Laatikkopyöriä kutsutaan hyvän pyöräilyyn
kannustavan infrastruktuurin indikaattorilajiksi, joten
laatikkopyörille suunnatut pysäköintipaikat on jotain, joiden
suosimista kannattaa harkita.

<figure id="rahtipyörän-teline">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/rahtipyörän-teline.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/rahtipyörän-teline.webp"
alt="Laatikkopyörän pysäköintiin tarkoitettu matala kaariteline."
title="Laatikkopyörän pysäköintiin tarkoitettu matala kaariteline."
width="880"
height="480"
/></a>
<figcaption>
<a href="#rahtipyörän-teline">Kuva 9</a>: Matala kaariteline, joka on
suunnattu laatikkopyörien käyttöön.
</figcaption>
</figure>

[Kuvassa 9](#rahtipyörän-teline) näkyy tyypillisiä laatikkopyörille
suunnattuja pysäköintipaikkoja. Pyörälle varattu tila on leveämpi mitä
tyypilliset runkolukituksen mahdollistavat telineiden välit ovat ja
eivät ole ihan seinässä kiinni, jotta laatikkopyörä on mahdollista
kiinnittää muustakin kuin eturenkaasta tällaiseen telineeseen. Lisäksi
tämä teline on [kuvan 9](#rahtipyörän-teline) mukaisesti matala,
johon tyypillisiä korkeammalla rungolla varustettuja pyöriä ei ole
niin helppo kiinnittää.

Laatikkopyörille varattuja paikkoja tulee pääkaupunkiseudulla niin
harvoin vastaan, että olen itse nähnhyt niitä juna-asemien yhteydessä
ja tässä yhdessä [kuvan 9](#rahtipyörän-teline)
päiväkodissa. Periaatteessa leveämmällä tilalla varustetut
runkolukittavat telineet ajavat laatikkopyörienkin tarpeen
pyöräpysäköinnille, mutta näissä on aina mahdollisuus, että
paikat menee pääasiassa muille kuin laatikkopyörien käyttäjille.

## Päiväkotien pyöräpysäköintitilanne

Päiväkoteja on pääkaupunkiseudulla suhteellisen tiheästi. Tämän voi
vaikka itse todistaa avaamalla [kuvan 10
kartan](#yleisnäkymä-päiväkodit). Mutta ei kuitenkaan ylenpalttisen
tiheästi. Jopa Helsingin kantakaupungin alueella voi joutua kulkemaan
puolesta kilometristä kilometriin lähimpään päiväkotiin. Sillä
oletuksella, että saa paikan lapselleen lähimmästä päiväkodista.

Vaikka itse lasten huoltajille ei suunniteltaisi
pitkäaikaispysäköintiin paikkoja, päiväkodin henkilökunta voi tulla
luonnollisesti kaueampaa töihin. Mutta edes heille ei ole tehty
paikkoja, joita he voisivat käyttää pyörien säilyttämiseen ja samalla
säilyttää mielenrauhansa.

<figure id="yleisnäkymä-päiväkodit">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/pyöräparkit-päiväkodit.html"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/yleisnäkymä-päiväkodit.webp"
alt="Päiväkodit kartalla. Runkolukittava pyöräpysäköinti korostettu"
title="Päiväkodit kartalla. Runkolukittava pyöräpysäköinti korostettu"
width="880"
height="707"
/></a>
<figcaption>
<a href="#yleisnäkymä-päiväkodit">Kuva 10</a>: Päiväkodit, joista
erikseen korostettu lähellä olevilla runkolukittavilla telineillä
varustetut.
</figcaption>
</figure>

[Kuvan 10](#yleisnäkymä-päiväkodit) [karttanäkymää
selaamalla](pyöräparkit-päiväkodit.html) voi saada kuvan siitä, miltä
[pääkaupunkiseudulla valitulla alueella olevat
päiväkodit](https://overpass-turbo.eu/s/1Umo) näyttävät
pyöräpysäköinnin suhteen. Näistä alueista saa tehtyä seuraavanlaisen
yhteenvedon:

* 646 päiväkotialuetta.
* 142 päiväkotia, joiden lähistöllä on runkolukittavat telineet 50
  metrin säteellä.
* 22 % päiväkodeista siten sisältää 50 metrin säteellä vähintään yhden
  runkolukittavan telineen.

Tällä kartalla näkyy 200 metrin säteellä jokaisesta päiväkodista
runkolukittavat telineet. Tästä voi kutakuinkin päätellä, että jos
vieressä ei ole runkolukittavaa telinettä, ei sitä ole myöskään
hieman kauempana.

22 % lukemasta voi päätellä, että pääkaupunkiseudun
varhaiskasvatuksessa pyöräpysäköinnin järjestelyt ei ole minkäänlainen
prioriteetti. Edes päiväkodin henkilökunnan pyöräpysäköinnin
suhteen. Tätä lukemaa vielä nostaa katujen varsien, viereisten
koulujen ja viereisten taloyhtiöiden pyörätelineet, jotka eivät ole
erikseen päiväkoteja varten suunnattuja. Puhumattakaan
päiväkotikohtaisista määristä, jotka monesti ovat auttamattoman
vähäisiä.

### Vertailua Helsingin kaupungin suunnitteluohjeisiin

Helsingin kaupungin suunnitteluohjeessa on eri mitoitukset erikseen
huoltajille ja henkilökunnalle pyöräpysäköinnin suhteen. Pääsääntönä
päiväkotien kohdalla on vaikuttanut kuitenkin olevan se, että ei ole
erillistä tilaa, jossa henkilökunta säilyttäisi pyöränsä. On tietenkin
mahdollista, että pienten rakennusten sisällä on erilliset tilat
pyörien säilytykselle. Mutta vähän epäilyttää, että tämä olisi tilanne
muuta kuin poikkeustapauksissa ja vain isommissa yksiköissä.

# Peruskoulut, ammattikoulut ja lukiot

Peruskoulusta lähtien lapsilla on yleensä mahdollisuus itsenäisesti
kouluun ilman huoltajien apua. Tämä tarkoittaa sen, että pyörällä
kulkemisen tapauksessa, pyörä jää koulun lähistölle päivän
ajaksi. Tämä nostaa paikkatarvetta verrattuna
päiväkoteihin. Ensimmäisen ja toisen asteen kouluja yhdistää myös se,
että nämä alkavat olla jo suhteellisen suuriä yksiköitä, joissa
opiskelijat lasketaan sadoissa
([Helsinki](https://hri.fi/data/fi/dataset/helsingin-perus-ja-2-asteen-opetuksen-oppilasmaarat-kouluittain),
[Vantaa](https://hri.fi/data/fi/dataset/vantaan-perusopetuksen-ja-lukioiden-oppilas-ja-opiskelijamaarat-kouluittain)).

## Koulujen pyöräpysäköintialueet

Koulujen suhteen on otettu selkeästi tarve pysäköidä polkupyöriä
jossain määrin tosissaan ja koulujen lähistöltä löytyy usein suuria
määriä pyörätelineitä. Suurin osa on edelleen tyypillisiä
kiekonväännintelineitä erilaisilla muunnoksilla, mutta välillä on
törmännyt ihan päteviin pyöräpysäköintiratkaisuihin.

<figure id="koulu-pyöräpysäköinti-pätevä">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/koulu-pyöräpysäköinti-pätevä.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/koulu-pyöräpysäköinti-pätevä.webp"
alt="Rivi pollarimuotoisia pyörätelineitä koulun pihassa."
title="Rivi pollarimuotoisia pyörätelineitä koulun pihassa."
width="880"
height="409"
/></a>
<figcaption>
<a href="#koulu-pyöräpysäköinti-pätevä">Kuva 11</a>: Vankalta tuntuvia
maahan upotettuja pollarimuotoisia pyörätelineitä koulun pihalla.
</figcaption>
</figure>

Runkolukittavia telineitä löytyy isompinakin alueina ja välillä vähän
erikoisempia [kuvan 11](#koulu-pyöräpysäköinti-pätevä)
pollariratkaisuja, joiden materiaalipaksuus todennäköisesti päihittää
tavanomaisimpien runkolukittavien telineiden materiaalipaksuuden.

<figure id="tiistilän-koulu-skuuttiparkki">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/tiistilän-koulu-skuuttiparkki.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/tiistilän-koulu-skuuttiparkki.webp"
alt="Skuuteille suunnattu pysäköintiteline, joka on pulteilla
kiinnitetty asvalttiin."
title="Skuuteille suunnattu pysäköintiteline, joka on pulteilla
kiinnitetty asvalttiin."
width="880"
height="389"
/></a>
<figcaption>
<a href="#tiistilän-koulu-skuuttiparkki">Kuva 12</a>: Tiistilän koulun
erillinen teline skuuteille.
</figcaption>
</figure>

Koulujen lähistöltä löytyy usein myös mahdollisuus pysäköidä muita
liikennevälineitä. Nykytrendien mukaisesti, myös skuuteille löytyy
joistain paikoista erillisiä parkkiratkaisuja, kuten [kuvasta
12](#tiistilän-koulu-skuuttiparkki) näkyy. Nämä ovat sikäli hyviä,
että skuutteja saa sopivalla telineratkaisulla vielä tiheämmin mitä
polkupyöriltä onnistuu. Harmi vaan, että
[kuvan 12](#tiistilän-koulu-skuuttiparkki) skuuttitelineratkaisu
näyttää olevan mutterinvääntimellä irroitettavaa mallia.

## Koulujen pyöräpysäköintitilanne

Peruskoulut, ammattikoulut ja lukiot ovat pääkaupunkiseudulla satojen
oppilaiden kokonaisuuksia, joista henkilökuntaakin löytyy
kymmenittäin. Johtuen koulujen oppilasmääristä ja toimintojen
monipuolisuudesta, nämä ovat usein laajahkolle alueelle rakennettuja
erillisiä kokonaisuuksia, joiden ympäristössä on tarvittaessa tilaa
useammallekin pyöräpysäköintialueelle. Henkilökunnalle ja oppilaille
erikseen.

<figure id="yleisnäkymä-koulut">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/pyöräparkit-koulut.html"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/yleisnäkymä-koulut.webp"
alt="Peruskoulut, ammattikoulut ja lukiot kartalla. Runkolukittava pyöräpysäköinti korostettu"
title="Peruskoulut, ammattikoulut ja lukiot kartalla. Runkolukittava pyöräpysäköinti korostettu"
width="880"
height="709"
/></a>
<figcaption>
<a href="#yleisnäkymä-koulut">Kuva 13</a>: Peruskoulut, ammattikoulut
ja lukiot, joista erikseen korostettu lähellä olevilla
runkolukittavilla telineillä varustetut.
</figcaption>
</figure>

[Kuvan 13](#yleisnäkymä-koulut) [karttanäkymää
selaamalla](pyöräparkit-koulut.html) voi saada kuvan siitä, miltä
[pääkaupunkiseudulla valitulla alueella olevat
peruskoulut, ammattikoulut ja
lukiot](https://overpass-turbo.eu/s/1Uml) näyttävät pyöräpysäköinnin
suhteen. Näistä alueista saa tehtyä seuraavanlaisen yhteenvedon:

* 416 koulualuetta.
* 192 koulua, joiden lähistöllä on runkolukittavat telineet 50 metrin
  säteellä.
* 46 % kouluista sisältää 50 metrin säteellä vähintään yhden
  runkolukittavan telineen.

Kouluja kun rakennetaan yleensä erillisille alueille, joten
päiväkotien apainen taloyhtiöiden pyörätelineiden kirjautuminen osaksi
tilastoja ei ole niin yleistä. Helsingin kantakaupungin
kadunvarsipysäköinnit nostaa koulujen runkolukittavien
pyöräpysäköintiratkaisujen tilastollista lukemaa hieman. Siitä
huolimatta, yli puolessa kouluista ei ole yhtään runkolukittavaa
telinettä näiden alueella tai 50 metrin säteellä koulusta.

Kokonaisuutena kouluissa on parempi tilanne kuin päiväkotien kohdalla,
sillä pyöräpysäköinti on edes jotenkin uskottavasti otettu kouluja
suunnitellessa huomioon. Mutta vielä on pitkä matka siihen, että
koulujen pihoilla lukot olisivat niitä heikoimpia lenkkejä
pyöräpysäköinnin suhteen. Eivätkä pyörätelineet.

# Ammattikorkeakoulut, yliopistot ja muut oppilaitokset

Korkea-asteen koulutus ja muu koulutus on pääasiassa rinnastettavissa
työpaikkoihin matka- ja pysäköintijärjestelyiden suhteen. Yksiköiden
koko on suhteellisen suuri ja pääkaupunkiseudulla on useampi
korkeakoulutuksen kampusalue, joihin kulkee päivittäin tuhansia
opiskelijoita ja henkilökuntaa.

## Korkea-asteen ja muiden oppilaitosten tilanne

<figure id="yleisnäkymä-korkea-aste">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/pyöräparkit-korkea-aste.html"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/yleisnäkymä-korkea-aste.webp"
alt="Ammattikorkeakoulut, yliopistot ja muut opetusta antavat
laitokset kartalla. Runkolukittava pyöräpysäköinti korostettu."
title="Ammattikorkeakoulut, yliopistot ja muut opetusta antavat
laitokset kartalla. Runkolukittava pyöräpysäköinti korostettu."
width="880"
height="709"
/></a>
<figcaption>
<a href="#yleisnäkymä-korkea-aste">Kuva 14</a>: Ammattikorkeakoulut,
yliopistot ja muut opetusta antavat laitokset, joista erikseen
korostettu lähellä olevilla runkolukittavilla telineillä varustetut.
</figcaption>
</figure>

[Kuvan 14](#yleisnäkymä-korkea-aste) [karttanäkymää
selaamalla](pyöräparkit-korkea-aste.html) voi saada kuvan siitä, miltä
[pääkaupunkiseudulla valitulla alueella olevat ammattikorkeakoulut, yliopistot ja muut opetusta tarjoavat tahot](https://overpass-turbo.eu/s/1Umr)
näyttävät pyöräpysäköinnin suhteen. Näistä alueista saa tehtyä
seuraavanlaisen yhteenvedon:

* 107 aluetta korkea-asteen ja muuhun koulutukseen.
* 66 aluetta, joiden lähistöllä on runkolukittavat telineet 50 metrin
  säteellä.
* 62 % alueista sisältää 50 metrin säteellä vähintään yhden
  runkolukittavan telineen.

Korkeakouluissa korostuu se, että yhden ison rakennuskohtaisen
pyöräpysäköintialueen sijaan, pyöräpysäköintimahdollisuuksia tehdään
usein eri puolille rakennusta eri sisäänkäyntien viereen. Nämä
koulutusta tarjoavat tahot toimivat pitkälti riippumattomina
kaupungista. Periaatteessa korkeakouluilta ja muilta opetusta
tarjoavilta tahoilta ei voi olettaa samanlaista kaupunkien
ohjeistuksen noudattamista pyöräpysäköintijärjestelyiden suhteen kuin
alemman tason oppilaitoksilta. Mutta näiden numeroiden valossa ainakin
runkolukituksen osalta tilanne on parempi kuin mitä kaikilla muilla
koulutusasteilla.

# Datan keräämisen ja analysoinnin sudenkuoppia

Vaikka dataa tuli parissa kuukaudessa kerättyä kentältä ja tämä
artikkeli tarjoaa yleiskatsauksen siihen dataan pyöräpysäköinnin
suhteen, mitä OpenStreetMapista löytyy, niin nämä asiat eivät ole
täysin ongelmattomia. Vähintään seuraavanlaiset ongelmat on tulleet
mieleen, kun tätä dataa tuli kerättyä ja ruksuteltua:

* Umpikorttelialueilla olevien päiväkotien lähelle ei monesti
  päässyt. Tämä on umpikorttelin ominaisuus ja sille on hanka tehdä
  mitään.
* Päiväkodit sisältää tässä datassa myös leikkipuistoja. Ja koulut ja
  korkea-asteen laitokset sisältää muitakin kuin peruskouluja,
  ammattikouluja, lukioita, ammattikorkeakouluja ja yliopistoja.
* Osaa OpenStreetMapista löytyvistä rakennuksista ja alueista ei ole
  enää olemassa.
* Kaikkia eri luokkiin kuuluvia rakennuksia ja alueita ei ole lisätty
  OpenStreetMapiin.
* Osa OpenStreetMapissa olevasta datasta on vanhentunutta, joka myös
  osaksi näkyy luoduissa karttanäkymissä joko vääränä tai puuttuvana
  tietona.
* Datan käsittelyssä on joitain paikkoja, joiden nimet saattaa
  vaikuttaa kummallisilta. En kuitenkaan jaksanut korjata näitä
  paremmiksi, sillä se olisi vaatinut liikaa poikkeustapauksia
  koodiin.
* Runkolukittavan telineen määritelmä on jokseenkin joustava. Lisäksi
  OpenStreetMapissa olevalla datalla ei ole näkemystä telineen
  laadun osalta. Paksusta teräksestä tehty riittävät välit sisältävä
  jykevä runkolukittava teline on samanarvoinen kuin
  mutterinvääntimellä avattava rimpulateräksestä tehty teline, johon
  yhteen väliin hädintuskin mahtuu yksi pyörä.
* Runkolukittavien telineiden sijainti tulee olla alle 50 metrin
  säteellä linnuntietä lähimmästä pisteestä, joka on kyseisen paikan
  alueen konveksi peitto. Tämä tarkoittaa sitä, että välillä
  viereisten taloyhtiöiden ja yleiset kaduilla olevat runkolukittavat
  telineet rekisteröityvät eri paikkojen pyörätelineiksi. Yleensä
  pyöräpysäköintimahdollisuuksia haetaan sisäänkäyntien läheltä, joten
  tämä vääristää tilastoja parempaan suuntaan.
* Datan keräämisessä on ollut oma tekijänsä sillä, että yhteen
  kohteeseen ei halua käyttää liikaa aikaa ja joissain tapauksissa
  jotain telineitä tai muita päteviä pysäköintiratkaisuja on voinut
  jäädä huomaamatta.

<figure id="työmaa-alue">
<a href="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/työmaa-alue.webp"><img
src="https://barro.gitlab.io/osm-diary-files/päiväkotien-ja-koulujen-pyöräpysäköintitilanne/työmaa-alue.webp"
alt="Työmaa-alue -kyltin sisältävä aita päiväkodin ympärillä."
title="Työmaa-alue -kyltin sisältävä aita päiväkodin ympärillä."
width="880"
height="454"
/></a>
<figcaption>
<a href="#työmaa-alue">Kuva 15</a>: Remontti meneillään yhdessä
päiväkodissa. Tästä ei voi etukäteen maastosta sanoa, että millaiset
pyörätelineratkaisut tulee lopulta käyttöön.
</figcaption>
</figure>

Lisäksi kun oli yli 1000 kohdetta läpikäytävänä, niin osa niistä oli
remontissa [kuvan 15](#työmaa-alue) mukaisesti. On mahdollista, että
näille remontoiduille alueille tulee paremmat pyörätelineratkaisut,
kunhan remontti on valmis. Näitä ei kuitenkaan tullut kirjattua
mihinkään jälkitarkastelua varten. Remontoidut alueet eivät myöskään
juurikaan vaikuttaisi tilastoihin.

# Käytetty ohjelmakoodi

Tämän artikkelin pohjana käytetty data ja tämän datan analysointiin ja
visualisointiin käytetty ohjelmakoodi löytyy osoitteesta
[https://gitlab.com/Barro/osm-diary-files/-/tree/main/public/p%C3%A4iv%C3%A4kotien-ja-koulujen-py%C3%B6r%C3%A4pys%C3%A4k%C3%B6intitilanne/skriptit-data?ref_type=heads](https://gitlab.com/Barro/osm-diary-files/-/tree/main/public/p%C3%A4iv%C3%A4kotien-ja-koulujen-py%C3%B6r%C3%A4pys%C3%A4k%C3%B6intitilanne/skriptit-data?ref_type=heads).
