#!/usr/bin/env python3

import argparse
import os
import sys

import geopandas as gpd

def main(argv):
    parser = argparse.ArgumentParser(prog=argv[0])
    parser.add_argument("buildings_convex_hulls")
    parser.add_argument("buildings_near_parkings")
    parser.add_argument("parkings_near_buildings")
    args = parser.parse_args(argv[1:])

    buildings_all = gpd.read_file(args.buildings_convex_hulls)
    buildings_parkings = gpd.read_file(args.buildings_near_parkings)
    parkings = gpd.read_file(args.parkings_near_buildings)

    buildings_total = len(buildings_all)
    buildings_parkings_total = len(buildings_parkings)
    parkings_total = parkings["capacity"].apply(int).sum()
    print(f"Buildings total: {buildings_total}")
    print(f"Buildings with parkings: {buildings_parkings_total} ({round(100.0 * buildings_parkings_total / buildings_total, 1)} %)")
    print(f"Parking capacity: {parkings_total}, {round(parkings_total / buildings_parkings_total, 1)} per building")
    return os.EX_OK

if __name__ == "__main__":
    sys.exit(main(sys.argv))
