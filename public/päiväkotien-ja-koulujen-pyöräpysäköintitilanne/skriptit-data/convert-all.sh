#!/usr/bin/env bash

set -euo pipefail

PYTHON_CMD=$HOME/venv/geopandas/bin/python

TARGETS=(päiväkodit koulut korkea-aste)

for TARGET in "${TARGETS[@]}"; do
    "$PYTHON_CMD" convert-buildings-to-convex-hulls.py --out "$TARGET"-convex-hulls.geojson "$TARGET".geojson
    "$PYTHON_CMD" buildings-near-parkings.py --out "$TARGET"-near-parking.geojson "$TARGET"-convex-hulls.geojson bicycle-parking.geojson 
    "$PYTHON_CMD" parkings-near-buildings.py --out parkings-near-"$TARGET".geojson "$TARGET"-convex-hulls.geojson bicycle-parking.geojson --max-distance 200.0
    "$PYTHON_CMD" parkings-near-buildings.py --out parkings-next-"$TARGET".geojson "$TARGET"-convex-hulls.geojson bicycle-parking.geojson --max-distance 50.0
    "$PYTHON_CMD" cleanup-buildings.py --out cleaned-"$TARGET".geojson "$TARGET".geojson
    "$PYTHON_CMD" kartta-parkkigonit.py --out pyöräparkit-"$TARGET".html cleaned-"$TARGET".geojson "$TARGET"-near-parking.geojson parkings-near-"$TARGET".geojson
done

for TARGET in "${TARGETS[@]}"; do
    echo "$TARGET"
    "$PYTHON_CMD" stats.py "$TARGET"-convex-hulls.geojson "$TARGET"-near-parking.geojson parkings-next-"$TARGET".geojson
done
