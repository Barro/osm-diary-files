#!/usr/bin/env python3

import folium
import folium.plugins
import numpy as np
import math
import geopandas as gpd
import shapely.geometry.point

m = folium.Map(location=(60.2132, 24.8600), tiles="cartodb positron", zoom_start=11)

folium.Rectangle(
    bounds=[[60.120381, 24.630048], [60.298650, 25.1223]],
    #bounds=[[60.13416, 24.67598], [60.26536, 25.1223]],
    color="black",
    fill=True,
    fill_color="grey",
).add_to(m)

# https://gis.stackexchange.com/questions/271733/geopandas-dissolve-overlapping-polygons
m.save("kartta-alue.html")
