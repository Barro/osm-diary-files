#!/usr/bin/env python3

import argparse

import folium
import folium.plugins
import numpy as np
import math
import geopandas as gpd
import shapely.geometry.point
import shapely.ops
import sys

def add_point_buffer(gdf):
    gdf = gdf.to_crs(3857)

    i_geometry = gdf.columns.get_loc("geometry")
    for i, (idx, row) in enumerate(gdf.iterrows()):
        if not isinstance(row.geometry, shapely.geometry.point.Point):
            continue
        gdf.iloc[i, i_geometry] = row.geometry.buffer(25)
    return gdf.to_crs(crs=4326)

def get_nearest_idx(idx, gdf):
    row = gdf.iloc[idx]
    removed = gdf.drop(index=idx)
    nearest_geoms = shapely.ops.nearest_points(row.geometry, removed.geometry.union_all())
    shortest_lines = removed.geometry.shortest_line(nearest_geoms[1])
    nearest_idx = shortest_lines.to_crs(crs=3857).length.idxmin()
    assert nearest_idx != idx
    return nearest_idx

def convex_hull_two(first, second):
    return gpd.GeoSeries([first, second]).union_all().convex_hull

def concave_hull_two(first, second):
    return gpd.GeoSeries([first, second]).union_all()

def merge_geometries(gdf, max_distance_m):
    visited_idx = set()
    merged_idx = {}
    i_geometry = gdf.columns.get_loc("geometry")
    for idx, row in gdf.iterrows():
        if idx in visited_idx:
            continue
        visited_idx.add(idx)
        nearest_idx = get_nearest_idx(idx, gdf)
        nearest_row = gdf.iloc[nearest_idx]
        line = gpd.GeoSeries(nearest_row.geometry).shortest_line(row.geometry)
        line_wgs84 = line.set_crs(crs=4326)
        line_pseudomercator = line_wgs84.to_crs(crs=3857)
        distance_m = line_pseudomercator.length.iloc[0]
        if distance_m < max_distance_m:
            visited_idx.add(nearest_idx)
            print("MERGE CCH", idx, nearest_idx, round(distance_m, 2))
            merged = merged_idx.get(idx, set()).union(merged_idx.get(nearest_idx, set()))
            merged.add(idx)
            merged.add(nearest_idx)
            merged_idx[idx] = merged
            merged_idx[nearest_idx] = merged
            geom_hull = concave_hull_two(row.geometry, nearest_row.geometry)
            for hull_idx in merged:
                hull_row = gdf.iloc[hull_idx]
                gdf.iloc[hull_idx, i_geometry] = geom_hull
        else:
            print("SKIP CCH", idx, nearest_idx, round(distance_m, 2), gdf.loc[idx].id)

    merged_rows = []
    queried_idx = set()
    for i, (idx, row) in enumerate(gdf.iterrows()):
        if idx in queried_idx:
            continue
        name = row["name"]
        if not name and "loc_name" in row:
            name = row["loc_name"]
        queried_idx.add(idx)
        if idx not in merged_idx:
            if not name:
                row_id = row["id"] or row["@id"]
                row["name"] = f"{row_id} (nimeämätön)"
            merged_rows.append(row)
            continue
        for combined_idx in merged_idx[idx]:
            potential_name = name
            if not potential_name:
                potential_name = gdf.loc[combined_idx, "name"]
            if not potential_name and "loc_name" in gdf.columns:
                potential_name = gdf.loc[combined_idx, "loc_name"]
            name = potential_name
            queried_idx.add(combined_idx)
        if name is None:
            row_id = row["id"] or row["@id"]
            row["name"] = f"{row_id} (nimeämätön)"
        else:
            row["name"] = name
        merged_rows.append(row)

    gdf_merged = gpd.GeoDataFrame(merged_rows)
    return gdf_merged.set_crs(crs=4326)

parser = argparse.ArgumentParser()
parser.add_argument("--out", required=True)
parser.add_argument("buildings")
args = parser.parse_args()

gdf_raw = gpd.read_file(args.buildings)
gdf_filtered = merge_geometries(gdf_raw, 1.0)
gdf_buffered = add_point_buffer(gdf_filtered)
gdf_buffered.to_file(args.out, driver="GeoJSON")
