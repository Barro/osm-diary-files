#!/usr/bin/env python3

import argparse

import folium
import folium.plugins
import numpy as np
import math
import geopandas as gpd
import shapely.geometry.point
import shapely.geometry.linestring
import shapely.ops
import sys

def add_point_buffer(gdf):
    gdf = gdf.to_crs(3857)

    i_geometry = gdf.columns.get_loc("geometry")
    for i, (idx, row) in enumerate(gdf.iterrows()):
        if not isinstance(row.geometry, shapely.geometry.point.Point) and not isinstance(row.geometry, shapely.geometry.linestring.LineString):
            continue
        gdf.iloc[i, i_geometry] = row.geometry.buffer(25)
    return gdf.to_crs(crs=4326)

def get_overlapping_geometries(concave_hulls, buildings):
    # Lienee parin pisteen tilanne. Ei oteta huomioon.
    concave_hulls = add_point_buffer(concave_hulls)
    buildings = add_point_buffer(buildings)
    concave_hulls = concave_hulls[concave_hulls.geometry.type != "LineString"]
    buildings = buildings[buildings.geometry.type != "LineString"]
    overlappings = concave_hulls.overlay(buildings, how="intersection")
    buildings["parking"] = False
    for idx, overlapping in overlappings.iterrows():
        buildings.loc[buildings.id == overlapping.id_2, ["parking"]] = True
    #import pdb; pdb.set_trace()
    return buildings

parser = argparse.ArgumentParser()
parser.add_argument("--out", required=True)
parser.add_argument("buildings")
parser.add_argument("buildings_with_parkings")
parser.add_argument("parkings_close_to_buildings")
args = parser.parse_args()

buildings = gpd.read_file(args.buildings)
buildings = add_point_buffer(buildings)
buildings_with_parkings = gpd.read_file(args.buildings_with_parkings)
parkings_close_to_buildings = gpd.read_file(args.parkings_close_to_buildings)

buildings = get_overlapping_geometries(buildings_with_parkings, buildings)

#pk = pk.to_crs(3857)

# for idx, row in pk.iterrows():
#     if not isinstance(row.geometry, shapely.geometry.point.Point):
#         continue
#     pk.loc[idx, "geometry"] = row.geometry.buffer(25)

# https://geopandas.org/en/stable/docs/reference/api/geopandas.GeoDataFrame.sjoin.html
# https://geopandas.org/en/stable/docs/reference/api/geopandas.GeoDataFrame.sjoin_nearest.html#geopandas.GeoDataFrame.sjoin_nearest

#pk = pk.to_crs(crs=4326)

mc = folium.plugins.MarkerCluster(
    options={
        "zoomToBoundsOnClick": False,
        "maxClusterRadius": 80,
        "disableClusteringAtZoom": 12,
    })

for idx, row in buildings_with_parkings.iterrows():
    point = None
    if isinstance(row.geometry, shapely.geometry.point.Point):
        point = row.geometry
    else:
        point = row.geometry.centroid
    name = None
    if row["name"] and not isinstance(row["name"], float):
        name = row["name"]
    if name is None:
        name = "(nimeämätön)"
    color="red"
    icon=None
    #icon=folium.Icon(color=color, icon="school")
    mc.add_child(
        folium.Marker(
            [point.y, point.x],
            tooltip=name,
            popup=f"<a href='https://openstreetmap.org/#map=18/{point.y}/{point.x}&amp;layers=C'>{name}</a>",
            icon=icon))

m = folium.Map(
    location=(60.2132, 24.8600),
    tiles="cartodb positron",
    zoom_start=12)

def parking_color(x, y):
    #import pdb; pdb.set_trace()
    if not x["properties"]["parking"]:
        y["fillColor"] = "red"
    return y


#update_location = JsCode("""\
#function update_location(e) {
#    
#}
#"""
#)


buildings_tooltip = folium.GeoJsonTooltip(
    fields=["name"], labels=False)
buildings_popup = folium.GeoJsonPopup(
    fields=["link"], labels=False)
idx_empty = buildings["name"].isna()
buildings.loc[idx_empty, "name"] = buildings.loc[idx_empty, "id"] + " (nimeämätön)"
y_centroids = buildings["geometry"].centroid.y.apply(str)
x_centroids = buildings["geometry"].centroid.x.apply(str)
buildings["link"] = "<a href='https://www.openstreetmap.org/#map=18/" + y_centroids + "/" + x_centroids + "&amp;layers=C'>" + buildings["name"] + "</a>"
geo_j_buildings = buildings[["geometry", "name", "id", "@id", "parking", "link"]].to_json()
geo_j_buildings = folium.GeoJson(
    data=geo_j_buildings,
    style_function=lambda x: parking_color(x, {"fillColor": "green"}),
    tooltip=buildings_tooltip,
    popup=buildings_popup)

geo_j_buildings.add_to(m)

parkings_tooltip = folium.GeoJsonTooltip(
    fields=["description"], labels=False)
parkings_popup = folium.GeoJsonPopup(
    fields=["description"], labels=False)
parkings_close_to_buildings["description"] = "TODO"
i_desciption = parkings_close_to_buildings.columns.get_loc("description")
for i, (idx, row) in enumerate(parkings_close_to_buildings.iterrows()):
    capacity = int(row["capacity"])
    parkings_close_to_buildings.iloc[i, i_desciption] = f"{capacity} runkolukittavaa paikkaa"
geo_j_parkings = parkings_close_to_buildings[["geometry", "description"]].to_json()
geo_j_parkings = folium.GeoJson(
    data=geo_j_parkings,
    style_function=lambda x: {"fillColor": "blue"},
    tooltip=parkings_tooltip,
    popup=parkings_popup)
geo_j_parkings.add_to(m)

m.add_child(mc)
m.save(args.out)
