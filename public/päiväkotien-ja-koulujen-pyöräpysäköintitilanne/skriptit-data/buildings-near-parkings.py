#!/usr/bin/env python3

import argparse

import folium
import folium.plugins
import numpy as np
import math
import geopandas as gpd
import shapely.geometry.point
import shapely.geometry
import shapely.ops
import sys

parser = argparse.ArgumentParser(
    description="Rakennukset pysäköintipaikkojen lähistöllä")
parser.add_argument("--out", required=True)
parser.add_argument("buildings", metavar="buildings-convex-hulls")
parser.add_argument("parkings")
args = parser.parse_args()

buildings = gpd.read_file(args.buildings)
parkings = gpd.read_file(args.parkings)

def get_buildings_close_to_parking(buildings, parkings, max_distance_m):
    parking_geometries_union = parkings.geometry.union_all()
    buildings = buildings.copy()

    #distances = gpd.GeoDataFrame({"@id": buildings["@id"]}, index=buildings.index)
    #distances["geometry"] = None
    #distances = distances.set_crs(crs=4326)
    buildings["nearline"] = None
    buildings["distance"] = 99999999.0

    for idx, building in buildings.iterrows():
        nearest_points = shapely.ops.nearest_points(building.geometry, parking_geometries_union)
        #nearest_line = building.geometry.shortest_line(nearest_points[1])
        nearest_line = shapely.geometry.LineString(nearest_points)
        buildings.loc[idx, "nearline"] = nearest_line

    buildings = buildings.set_geometry("nearline").set_crs(crs=4326)
    buildings = buildings.to_crs(crs=3857)
    buildings["distance"] = buildings.nearline.length
    #buildings["distance"] = buildings.nearline.to_crs(crs=3857).length
    #distances[distances["distance"] > 50, "distance"] = None

    save_buildings = buildings[buildings["distance"] < max_distance_m]
    #import pdb; pdb.set_trace()
    save_buildings = save_buildings[["id", "@id", "name", "geometry"]]
    return save_buildings

save_buildings = get_buildings_close_to_parking(buildings, parkings, 50.0)
save_buildings.to_file(args.out, driver="GeoJSON")
