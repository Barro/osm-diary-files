#!/usr/bin/env python3

import argparse

import folium
import folium.plugins
import numpy as np
import math
import geopandas as gpd
import shapely.geometry.point
import shapely.ops
import sys

parser = argparse.ArgumentParser(
    description="Rakennuksista konvekseja peittoja")
parser.add_argument("--out", required=True)
parser.add_argument("buildings")
args = parser.parse_args()

buildings = gpd.read_file(args.buildings)

def get_nearest_idx(idx, gdf):
    row = gdf.loc[idx]
    removed = gdf.drop(index=idx)
    nearest_geoms = shapely.ops.nearest_points(row.geometry, removed.geometry.union_all())
    shortest_lines = removed.geometry.shortest_line(nearest_geoms[1])
    nearest_idx = shortest_lines.to_crs(crs=3857).length.idxmin()
    assert nearest_idx != idx
    return nearest_idx
    # nearest_row = removed.loc[removed["geometry"].intersects(nearest_geoms[1])]
    # if len(nearest_row) == 0:
    #     import pdb; pdb.set_trace()
    # return nearest_row

def convex_hull_two(first, second):
    return gpd.GeoSeries([first, second]).union_all().convex_hull

def convex_hull_nearest(buildings):
    visited_idx = set()
    merged_idx = {}

    buildings = buildings.set_crs(crs=4326)
    buildings["geometry"] = buildings.geometry.convex_hull

    for i, (idx, row) in enumerate(buildings.iterrows()):
        if idx in visited_idx:
            continue
        visited_idx.add(idx)
        nearest_idx = get_nearest_idx(idx, buildings)
        nearest_row = buildings.loc[nearest_idx]
        line = gpd.GeoSeries(nearest_row.geometry).shortest_line(row.geometry)
        line_wgs84 = line.set_crs(crs=4326)
        line_pseudomercator = line_wgs84.to_crs(crs=3857)
        distance_m = line_pseudomercator.length.iloc[0]
        #if row.id == "way/322084471":
        #    import pdb; pdb.set_trace()
        if distance_m >= 25.0:
            print("SKIP CXH", idx, nearest_idx, round(distance_m, 2), buildings.loc[idx].id)
            continue
        print("MERGE CXH", idx, nearest_idx, round(distance_m, 2))
        merged = merged_idx.get(idx, set()).union(merged_idx.get(nearest_idx, set()))
        merged.add(idx)
        merged.add(nearest_idx)
        merged_idx[idx] = merged
        merged_idx[nearest_idx] = merged
        convex_hull = convex_hull_two(row.geometry, nearest_row.geometry)
        for convex_idx in merged:
            convex_row = buildings.loc[convex_idx]
            buildings.loc[buildings.index == convex_row.name, "geometry"] = convex_hull

    merged_rows = []
    queried_idx = set()
    for i, (idx, row) in enumerate(buildings.iterrows()):
        if idx in queried_idx:
            continue
        name = row["name"]
        if "loc_name" in row:
            name = name or row["loc_name"]
        queried_idx.add(idx)
        if idx not in merged_idx:
            if name is None:
                row_id = row["id"] or row["@id"]
                row["name"] = f"{row_id} (nimeämätön)"
            merged_rows.append(row)
            continue
        for combined_idx in merged_idx[idx]:
            potential_name = buildings.loc[combined_idx, "name"]
            if potential_name is None and "loc_name" in buildings.columns:
                potential_name = buildings.loc[combined_idx, "loc_name"]
            if name is None and potential_name is not None:
                name = potential_name
            queried_idx.add(combined_idx)
        # if row["id"] == "way/513948692":
        #     import pdb; pdb.set_trace()
        if name is None:
            row_id = row["id"] or row["@id"]
            row["name"] = f"{row_id} (nimeämätön)"
        else:
            row["name"] = name
        merged_rows.append(row)
    buildings = gpd.GeoDataFrame(merged_rows)
    buildings = buildings.set_crs(crs=4326)
    buildings = buildings.reindex()
    return buildings

len_original = len(buildings)
len_prev = 0
while len_prev != len(buildings):
    len_prev = len(buildings)
    buildings = convex_hull_nearest(buildings)
print(len_original, len(buildings))

def add_point_buffer(gdf):
    gdf = gdf.to_crs(3857)

    for idx, row in gdf.iterrows():
        if not isinstance(row.geometry, shapely.geometry.point.Point):
            continue
        gdf.loc[gdf.index == row.name, "geometry"] = gdf.geometry.buffer(25)
    return gdf.to_crs(crs=4326)

buildings = add_point_buffer(buildings)

# https://gis.stackexchange.com/questions/271733/geopandas-dissolve-overlapping-polygons
buildings.to_file(args.out, driver="GeoJSON")
