#!/usr/bin/env python3

import argparse

import folium
import folium.plugins
import numpy as np
import math
import geopandas as gpd
import shapely.geometry.point
import shapely.geometry
import shapely.ops
import sys

def add_point_buffer(gdf):
    gdf = gdf.to_crs(3857)
    for i, (idx, row) in enumerate(gdf.iterrows()):
        if not isinstance(row.geometry, shapely.geometry.point.Point):
            gdf.iloc[i, gdf.columns.get_loc("geometry")] = row.geometry.buffer(5)
            continue
        gdf.iloc[i, gdf.columns.get_loc("geometry")] = row.geometry.buffer(10)
    return gdf.to_crs(crs=4326)

def add_description(gdf):
    gdf["description"] = ""
    i_description = gdf.columns.get_loc("description")
    for i, (idx, row) in enumerate(gdf.iterrows()):
        gdf.iloc[i, i_description] = "deksi"
    return gdf

parser = argparse.ArgumentParser()
parser.add_argument("--out", required=True)
parser.add_argument("--max-distance", type=float, required=True)
parser.add_argument("buildings")
parser.add_argument("parkings")
args = parser.parse_args()

buildings = gpd.read_file(args.buildings)
parkings = gpd.read_file(args.parkings)
parkings = parkings[~parkings["capacity"].isna()].reindex()

def get_parking_close_to_buildings(buildings, parkings, max_distance_m):
    buildings = buildings.to_crs(crs=3857)
    parkings = parkings.to_crs(crs=3857)
    return parkings.sjoin_nearest(
        buildings,
        how="inner",
        max_distance=max_distance_m)

save_parkings = get_parking_close_to_buildings(buildings, parkings, args.max_distance)
save_parkings = save_parkings.to_crs(crs=4326)
save_parkings = add_point_buffer(save_parkings)
#save_parkings = add_description(save_parkings)
if "capacity" not in save_parkings.columns:
    save_parkings["capacity"] = save_parkings["capacity_left"]
save_parkings.to_file(args.out, driver="GeoJSON")
