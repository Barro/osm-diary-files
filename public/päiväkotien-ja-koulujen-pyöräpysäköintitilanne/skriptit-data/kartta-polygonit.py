#!/usr/bin/env python3

import argparse

import folium
import folium.plugins
import numpy as np
import math
import geopandas as gpd
import shapely.geometry.point
import shapely.ops
import sys

parser = argparse.ArgumentParser()
parser.add_argument("--out", required=True)
parser.add_argument("data")
args = parser.parse_args()

pk = gpd.read_file(args.data)
#pk = pk.to_crs(3857)

# for idx, row in pk.iterrows():
#     if not isinstance(row.geometry, shapely.geometry.point.Point):
#         continue
#     pk.loc[idx, "geometry"] = row.geometry.buffer(25)


#pk = pk.to_crs(crs=4326)
pk_popup = folium.GeoJsonPopup(
    fields=["link"], labels=False)
tooltip = folium.GeoJsonTooltip(fields=["name"], labels=False)
idx_empty = pk["name"].isna()
pk.loc[idx_empty, "name"] = pk.loc[idx_empty, "id"] + " (nimeämätön)"
pk["link"] = "<a href='https://www.openstreetmap.org/" + pk["id"] + "'>" + pk["name"] + "</a>"

geo_j = pk[["geometry", "name", "link"]].to_json()
geo_j = folium.GeoJson(
    data=geo_j,
    style_function=lambda x: {"fillColor": "orange"},
    tooltip=tooltip,
    popup=pk_popup,
)
m = folium.Map(location=(60.2132, 24.8600), tiles="cartodb positron", zoom_start=11)
geo_j.add_to(m)
m.save(args.out)
