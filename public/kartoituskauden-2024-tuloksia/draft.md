# Pyöräpysäköintipaikkojen kartoituskauden 2024 tuloksia

Nyt sää alkaa olla sen verran kylmä, ja välillä luminen, että voidaan
todeta, että on aika siirtää pyöräpysäköintipaikkojen kartoitus
talvilevolle ja jatkaa urakkaa isommassa määrin sitten, kun pakkaset
ovat taas väistyneet.

Kauden 2024 projektina itselläni oli seuraavanlaisia asioita
läpikäytävänä omalta [tarkastelualueeltani](https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/kartta-alue.html):

* Käydään määräpuutteelliset pyöräpysäköintialueet läpi ja
  täydennetään niihin olennaisimmat tiedot.
* Tarkistetaan pyöräpysäköintialueet, joihin ei ole koskettu sitten
  vuoden 2018 alun jälkeen, että onko mikään merkittävästi muuttunut.
* Käydään juna-asemien pyöräpysäköintimahdollisuudet läpi.
* Aloitetaan koulujen ja päiväkotien pyöräpysäköintimahdollisuuksien
  läpikäynti. Tästä tuli kirjoitettua [Päiväkotien ja koulujen
  pyöräpysäköintitilanne](https://www.openstreetmap.org/user/Barro/diary/405617)
  -artikkelissa vähän pidemmälti.

Näissä projekteissa huomiona se, että kun merkitsen
pyöräpysäköintimahdollisuuksia kartalle, en ota huomioon
kiekonväännintelineiden olemassaoloa. Paitsi jos kyseinen
pysäköintialue on jonkun muun lisäämä kartalle. Tähän syynä se, että
kyseiset telineet on huonompi vaihtoehto pyörän pysäköinnin suhteen
kuin kiinnittää pyörä aitaan tai tolppaan. Ja eivät toimi
suunnitellusti isolle osalle pyörätyypeistä, jotka ei osu siihen
yhteen platoniseen ideaaliipyörätyyppiin, jolle kyseinen telinemalli
on mitoitettu.

# Huomiota kiinnittäneitä asioita

Kartoituskauden aikana jotkut asiat kiinnitti huomiota positiivisessa
määrin.

<figure id="#kaisantunnelin-pyöräparkki">
<a href="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/kaisantunnelin-pyöräparkki.webp"><img
src="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/kaisantunnelin-pyöräparkki.webp"
alt="Kaksikerrostelineitä kolmessa rivissä."
title="Kaksikerrostelineitä kolmessa rivissä."
width="840"
height="273"
/></a>
<figcaption>
<a href="#kaisantunnelin-pyöräparkki">Kuva 1</a>: Satoja
kaksikerrostelineitä Kaisantunnelin pyörätallissa.
</figcaption>
</figure>

Kaksikerrostelineitä (esimerkki [kuvassa
1](#kaisantunnelin-pyöräparkki)) on lähdetty hankkimaan juna-asemille
oikein urakalla. Näissä on samat ongelmat käytön suhteen kuin
kiekonväännintelineissä, mutta niiden määrät tulee kuitenkin kirjattua
kartalle. Helsingin kaupunki on lisäksi saanut valtiolta avustusta
juna-asemien pyöräpysäköinnin parantamiseksi, joka on toteutettu
isoilta osin näitä kaksikerrostelineitä, runkolukittavia
pyörätelineitä ja laatikkopyörille suunnattuja telineitä käyttäen.

<figure id="laatikkopyöräteline">
<a href="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/laatikkopyöräteline.webp"><img
src="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/laatikkopyöräteline.webp"
alt="Laatikkopyörille tarkoitettu teline tavallisemmille pyörille
suunnattujen telineiden vieressä."
title="Laatikkopyörille tarkoitettu teline tavallisemmille pyörille
suunnattujen telineiden vieressä."
width="840"
height="345"
/></a>
<figcaption>
<a href="#laatikkopyöräteline">Kuva 2</a>: Laatikkopyörille
tarkoitettu pyöräteline Huopalahden juna-asemalla.
</figcaption>
</figure>

Rahtipyörille suunnattuja telinetyyppejä on alkanut löytymään
muutamilta juna-asemilta. Esimerkki tällaisesta on Huopalahden
asemalta [kuvassa 2](#laatikkopyöräteline), jossa tyypillisempien
runkolukittavien telineiden lisäksi löytyy myös tällainen
laatikkopyörille tarkoitettu teline.

<figure id="katettu-kiekonväännin">
<a href="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/katettu-kiekonväännin.webp"><img
src="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/katettu-kiekonväännin.webp"
alt="Viisipaikkainen kiekonväänninpyöräteline katoksen alla
päiväkodin piha-aidan ulkopuolella. Katoksessa tilaa myös
lastenvaunuille."
title="Viisipaikkainen kiekonväänninpyöräteline katoksen alla
päiväkodin piha-aidan ulkopuolella. Katoksessa tilaa myös
lastenvaunuille."
width="840"
height="427"
/></a>
<figcaption>
<a href="#katettu-kiekonväännin">Kuva 3</a>: Katettu
kiekonväännintyyppinen pyöräteline päiväkodin vieressä.
</figcaption>
</figure>

Päiväkotien, koulujen ja muiden oppilaitosten pyöräpysäköinnistä tuli
tehtyä vähän isompi urakka. Siinä tuli todettua, että erityisesti
[päiväkotien pyöräpysäköintitilanne on
huono](https://www.openstreetmap.org/user/Barro/diary/405617). [Kuvassa
3](#katettu-kiekonväännin) on esimerkki yhdentyyppisestä
heikohkonpuoleisesta telineratkaisusta päiväkodin edustalla.

# Pyöräpysäköintimäärien kehitys

Pyöräpysäköintimäärien kehitys OpenStreetMapin datassa on ollut vuoden
2024 kartoituskaudella ollut voimakkaan nousujohteista. Talven
jäljiltä kun lähdin käymään paikkamääräpuutteisia
pyöräpysäköintialueita läpi, niin tämä antoi hyvän pohjan
viimevuotisen datan päälle täydentämiselle ([kuva
4](#puuttuu-määrät)).

<figure id="puuttuu-määrät">
<a href="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/puuttuu-määrät-20210101-20241125.webp"><img
src="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/puuttuu-määrät-20210101-20241125.webp"
alt="Kuvaaja, josta näkyy paikkamääräpuutteisten
pyöräpysäköintialueiden lukumäärä ajan saatossa tarkastelualueella."
title="Kuvaaja, josta näkyy paikkamääräpuutteisten
pyöräpysäköintialueiden lukumäärä ajan saatossa tarkastelualueella."
width="840"
height="403"
/></a>
<figcaption>
<a href="#puuttuu-määrät">Kuva 4</a>: Paikkamääräpuutteellisten
pyöräpysäköintialueiden määrä OpenStreetMapin tietokannassa
vuodesta 2021 lähtien tarkastelualueella.
</figcaption>
</figure>

[Kuvan 4](#puuttuu-määrät) kuvaajasta näkyy, että OpenStreetMapin
dataan on tullut talven mittaan muiden kartoittajien ansiosta
pyöräparkkeja tasaista tahtia, joita sitten lämmenneiden säiden myötä
sai täydennettyä tiedoiltaan kattavammaksi.

<figure id="pyöräparkit-määrät">
<a href="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/pyöräparkit-määrät-20210101-20241125.webp"><img
src="https://barro.gitlab.io/osm-diary-files/kartoituskauden-2024-tuloksia/pyöräparkit-määrät-20210101-20241125.webp"
alt="5 yleisimmän pyöräpysäköintipaikkatyypin paikkojen määrän kehitys
ajan saatossa tarkastelualueella."
title="5 yleisimmän pyöräpysäköintipaikkatyypin paikkojen määrän kehitys
ajan saatossa tarkastelualueella."
width="840"
height="403"
/></a>
<figcaption>
<a href="#pyöräparkit-määrät">Kuva 5</a>: Pyöräpysäköintipaikkojen kehitys
telinetyypeittäin vuodesta 2021 OpenStreetMapin tietokannassa
tarkastelualueella.
</figcaption>
</figure>

[Kuvassa 5](#pyöräparkit-määrät) näkyy kehitys sen suhteen, että
miten [eri tyyyppisten pyöräpysäköintipaikkojen määrä on kehittynyt
ajan
myötä](https://wiki.openstreetmap.org/wiki/Key:bicycle_parking). Epäilen,
että
[*safe_loops*](https://wiki.openstreetmap.org/wiki/Tag:bicycle_parking%3Dsafe_loops)-tunnisteella
olevat runkolukituksen tarjoavat kiekonväännintyyppiset pyörätelineet
ovat merkittävästi yleistyneet datassa. Todennäköisesti datan
keräämisen ja keräämisen laadun paranemisen
seurauksena. [*two-tier*](https://wiki.openstreetmap.org/wiki/File:Bicis_a_l%27estaci%C3%B3_de_Leiden.JPG)-tunnisteella
olevat kaksikerroksiset kiekonväännintyyppiset pyörätelineet ovat
pääasiassa lisääntyneet sen takia, että niitä on alkanut ilmestymään
merkittäviä määriä juna-asemille.

# Loppupohdintoja

Runkolukittavia telineitä alkaa olla sellainen määrä
pääkaupunkiseudulla OpenStreetMapin tietokannassa, että olen voinut
omilla reissuillani jo muutamaan otteeseen hyödyntää aikaisemmin
tehtyjä kartoituksia. Eli ainakin omassa käytössäni tästä tehdystä
työstä alkaa hiljalleen olla yllättävää hyötyä. Pitää vain toivoa,
että pääkaupunkiseudun julkiset ja yksityiset toimijat alkaa
korvaamaan olemassaolevia heikkolaatuisia telineitään
monikäyttöisemmillä runkolukittavilla telineillä. Jotta jossain
vaiheessa voisi vain mennä pyörällä uusiin paikkoihin ilman, että
tarvitsee turvautua OpenStreetMapin dataan löytääkseen pätevän paikan
pyörällensä.
