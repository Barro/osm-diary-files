## Määrätiedoiltaan puutteelliset pyöräparkit

Kuten aikaisemmassa [Pyöräpysäköintimahdollisuudet
kartalle](https://www.openstreetmap.org/user/Barro/diary/401720)
-artikkelissa kirjoitin, kun pyöräilen tavanomaisilla
asiointimatkoilla, niin pidän silmäni auki sen osalta, että josko
havaitsisi uusia OpenStreetMapiin lisättäviä
[pyörätelineitä](https://wiki.openstreetmap.org/wiki/Key:bicycle_parking).

<figure id="kaikki-pyöräpysäkit-helsinki-bbox">
<a href="https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/overpass-turbo-pyöräpysäköinnit-tarkastelualueella.webp"><img
src="https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/overpass-turbo-pyöräpysäköinnit-tarkastelualueella.webp" height="440"
alt="Overpass turbo näyttämässä pyöräpysäkkejä pääasiassa Helsinkiin rajatulla alueella."
title="Overpass turbo näyttämässä pyöräpysäkkejä pääasiassa Helsinkiin rajatulla alueella."
/></a>
<figcaption>
Kuva 1: OpenStreetMapista rajatulla alueella löytyvät
pyöräpysäköintialueet Helsingin seudulla.
</figcaption>
</figure>

Tänä keväänä lähdin erikseen täydentämään OpenStreetMapissa jo
olemassaolevaa dataa. Otin projektiksi hakea kaikki OpenStreetMapista
löytyvät kapasiteettitietopuutteiset pyöräptelineet noin suunnilleen
[kuvassa 1 näkyvältä alueelta](#kaikki-pyöräpysäkit-helsinki-bbox). Ja
käydä nämä läpi ja täydentää pyöräpysäkkien
[kapasiteetti](https://wiki.openstreetmap.org/wiki/Key:capacity),
[tyyppi](https://wiki.openstreetmap.org/wiki/Key:bicycle_parking) ja
se, että onko nämä [katettuja vaiko
ei](https://wiki.openstreetmap.org/wiki/Key:covered). Olemassaolon
lisäksi. Siinä sivussa tuli myös lisättyä kasoittain uusia telineitä
kartalle, kun päädyin paikkoihin, joihin en olisi muuten mennyt.

## Pyöräpysäköintipaikkojen määrän kehitys

OpenStreetMapin datasta voi sopivin kyselyin hakea, että miten
erityyppisten pyöräpysäköintipaikkojen lukumäärä on muuttunut ajan
saatossa. [Kuva 2](pyöräpysäköintipaikkojen-määrän-kehitys) näyttää sen, että
miten tarkkailemallani alueella ajan saatossa tämä määrä vuoden 2021
alusta vuoden 2023 syyskuun 18. päivään mennessä on kehittynyt.

<figure id="pyöräpysäköintipaikkojen-määrän-kehitys">
<a href="https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/pyöräpysäköintipaikat-määrät-20210101-20230918.webp"><img
src="https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/pyöräpysäköintipaikat-määrät-20210101-20230918.webp" height="440"
alt="5 yleisimmän telinetyypin paikkamäärän kehitys tammikuusta syyskuuhun 2023."
title="5 yleisimmän telinetyypin paikkamäärän kehitys tammikuusta syyskuuhun 2023."
/></a>
<figcaption>
Kuva 2: yleisimpien pyörätelinetyyppien paikkojen lukumäärän kehitys
OpenStreetMapissa vuodesta 2021 asti. Huomaa, että tämä valehtelee
verrattuna todellisuuteen, sillä <code>wall_loops</code>-tyyppiä on
oikeasti luonnossa paljon enemmän.
</figcaption>
</figure>

Yleisin telinetyyppi tässä datassa on `stands`-tyypin teline, joka
ilmaisee, että teline on runkolukittavaa sorttia. Siinä myös näkyy
uudentyyppisten kaksikerroksisten `two-tier`-tyypin telineiden
hidas lisääntyminen ja runkolukitusmahdollisuuden tarjoavien
kiekonvääntimien mukaanotto `safe_loops`-arvolla. Harmi vaan, että
`safe_loops`-tyypin telineet eivät ole oikein missään
lisäyskäyttöliittymässä oletuksena valittavissa.

Jottei kenellekään jää epäselväksi, tämä on tyyppiesimerkki siitä,
miten data valehtelee. Tässä on selkeä valintaharha, jonka olen myös
aikaisemmin maininnut. Jätän surkeat kiekonväännintyyppiset
pyörätelineet tarkoituksella lisäämättä kartalle, ellei kyseessä ole
poikkeustapaus. Lisäksi tässä ei ole lisättynä taloyhtiöiden
pyöräkellareita ja muita paikkoja, joihin ei ilman avainta pääse. Eikä
myöskään pyöräparkkeja, joiden paikkojen lukumäärä ei ole helposti
kvantifioitavissa.

Liitteenä [pyöräpysäköintipaikkojen määrän kehityksen sisältävä
data](https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/pyöräparkit-historia/dated-totals.csv) ja (muokkausta vaativat) [ohjelmat tämän datan
noutamiseksi ja käsittelemiseksi](https://gitlab.com/Barro/osm-diary-files/-/tree/main/public/py%C3%B6r%C3%A4pys%C3%A4k%C3%B6intiprojekti-kapasiteettit%C3%A4ydennysten-osalta-pulkassa/py%C3%B6r%C3%A4parkit-historia).

### Pyöräparkkidatan ylläpito

Kun kartalla on nyt parisentuhatta pyöräparkkia, niin kysymys kuuluu,
että miten näitä kannattaa ylläpitää? Iso osa lisätyistä telineistä on
liikuteltavia, joten näiden paikkojen pysyvyys on vähintäänkin
kyseenalainen ajan saatossa. Myös maahan pultattuja tai kiinteästi
asennettuja telineitä katoaa erinäisistä syistä, kuten irrottamisen,
hajottamisen tai infrastruktuurimuutosten seurauksena. Vähenemisen
vastapainoksi telineitä voi luonnollisesti tulla lisää samaan
kohtaan tai tyyppi voi vaihtua toiseksi, erityisesti siirrettävien
telineiden osalta.

Yhtenä ylläpitotyökaluna on OpenStreetMapin paikkapisteen
[muutosaikaleima](https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL#Meta_Data_Operators). Kun
pyöräparkkitiedoissa jokin asia muuttuu, niin OpenStreetMapin
aikaleima muuttuu myös sen seurauksena. Tämä tarjoaa helpon tavan
hakea kauan aikaa sitten muokattuja paikkapisteitä
seuraavantyyppisellä Overpass-kyselyllä:

<pre><code>node
  ["amenity"="bicycle_parking"](if: timestamp() < date("2019-01-01"))
  (60.13416, 24.67598, 60.26536, 25.1223);
out;</code></pre>

Aikaleima voi muuttua jonkun pienen korjauksen takia, mutta
aikaleimakyselyllä saanee riittävän hyvällä tarkkuudella selville,
että mitä kannattaa käydä tarkistamassa. Lisäksi
[StreetCompletessa](https://streetcomplete.app/) on [erillinen tehtävä
pyöräpysäköinnin kapasiteetin
kyselemiseksi](https://wiki.openstreetmap.org/wiki/StreetComplete/Quests),
jonka tietoja kysytään uudestaan 4 vuoden välein. Tämä ei kuitenkaan
uudelleenkysele pyöräpysäköinnin muita ominaisuuksia ja on tällä
hetkellä rajoittunut vain `bicycle_parking=stands` ja
`bicycle_parking=wall_loops` -tyyppisiin pyörätelineisiin.

OpenStreetMapissa on myös käytäntö sille, että mitä tehdä, jos jonkin
kohteen tiedot täsmäävät kartassa oleviin ja mitään ei tarvitse
muuttaa. Se on
[`check_date`](https://wiki.openstreetmap.org/wiki/Key:check_date)-tietueen
asettaminen nykyiseen päivämäärään. Harmi vaan, että ainakaan
[OsmAnd](https://osmand.net/docs/user/plugins/osm-editing) ei tarjoa
tätä tunnettujen tietueiden nimien listalla, joten sen käyttö ei
OsmAndissa ole niin helppoa kuin haluaisi.

## Havaintoja

Vaikka tässä datassa kiekonväännintyyppiset telineet ei ole se
lukumäärältään yleisin telinemalli, niitä kuitenkin löytyy
todennäköisesti yhä eniten tarjoamaan pyörille
pysäköintimahdollisuuksia. Erityisesti kun itse en lisää näitä
telineitä OpenStreetMapiin muuta kuin poikkeustapauksissa. Tässä
suhteessa OpenStreetMapin data on selkeästi harhaista verrattuna
todellisuuteen, mutta sentään parempaan suuntaan kalleellaan.

Kun kohtaa taloyhtiöitä, joissa pyöräpysäköintiin on selkeästi
telineiden osalta panostettu, niin ne suurimmaksi osaksi tuntuu olevan
alueilla, jotka näyttää rakennetuilta tässä luokkaa viimeisen
vuosikymmenen-parin aikana. Vanhempiakin taloja löytyy, jossa pihalla
on kunnon telineet, mutta ne on enemmänkin poikkeus kuin sääntö.

Melkein mikään käyttämistäni karttasovelluksista tai sivustoista ei
oikein näytä viivamaisia pyöräpysäköintejä. Nämä tuli sitten muutettua
alueiksi, jotta, OpenStreetMapissa oleva data olisi myös oikeasti
käyttökelpoista. Eikä aiheuta turhaa ihmettelyä, kun sattuu lisäämään
toiseen otteeseen jonkun viivaksi merkityn jo olemassaolevan
pyöräparkin.

### Jatkoprojekteja

Tämä projektihan tavallaan jatkuu koko ajan, kun muut OpenStreetMapin
käyttäjät lisäilee pyöräpysäköintejä kartalle ja ei välttämättä kirjaa
kaikkia kiinnostavia tietoja osana paikkapisteen lisäystä. Vielä on
silti yhä mahdollista tehdä isompia rykäisyjä OpenStreetMapin datan
parantamiseksi.

Alunperin rajasin alueen, jolla teen tätä projektia sellaiseksi, että
se kattaa noin suunnileen paikat, joiden lähistöllä kulkisin
muutoinkin säännöllisen epäsäännöllisesti. Mutta siinä vaiheessa kun
teki erikseen 60-70 kilometrin matkoja paikkoihin, joihin ei sillä
hetkellä ollut muuta tarvetta mennä, niin tuli myös mieleen, että
tarkkailtavan alueen laajentaminen ulottumaan pidemmälle länteen ja
pohjoiseen voisi olla paikallaan. Itäisessä Helsingissä on tullut
käytyä riittävän usein riittävän syvällä, että alueen laajentaminen
sinne ei houkuttele.

Valitsin olemassaolevasta datasta sellaiset paikkapisteet, joista
puuttui paikkojen määrän kertova `capacity`-tietue. Näitä
puuttuvaistietuepaikkapisteitä voisi ainakin metsästää sen osalta,
että käydään läpi paikat seuraavien tietueiden osalta:

* Katettuna olosta kertova `covered`-tietue. Tämä tieto voi auttaa
  jotakuta hakemaan lähialueelta mahdollisesti katettuja paikkoja
  sateisella säällä.
* Pyöräpysäköinnin typin kertova `bicycle_parking`-tietue puuttuu myös
  välillä, vaikka kapasiteetti olisi tiedossa.
* Jotkut `bicycle_parking=rack`-arvon tietueet olisi usein parempi
  muuttaa `stands` tai `wall_loops` -arvoon.
* Pääsyn täysin kieltävällä `access=no`-arvolla varustettujen
  paikkojen läpikäynti. Nämä yleensä olisi parempi varustaa
  `access=private`-arvolla.

Aikaisemmassa luvussa kuvattu pyöräpysäköintitietojen ylläpito
läpikäymällä kauan aikaa sittten muokattuja paikkoja läpi on myös
mahdollinen oma projektinsa.

#### Eri paikkatyyppien järjestelmällinen läpikäynti

Vähemmän yllättäen huomasin, että pyörätelineitä löytyy paikoista,
joihin ihmiset luonnollisestikin menevät viettämään aikaa ja joita
ylläpitävät tahot (yleensä kaupunki) huomioivat pyöräpysäköinnin
tarpeet. Näitä paikkoja voisi järjestelmällisesti läpikäydän ainakin
seuraavantyyppisten paikkojen osalta.

* Leikkipuistot ([`leisure=playground`](https://overpass-turbo.eu/s/1AAx)).
* Urheilukentät ([`leisure=pitch`, `leisure=stadium`](https://overpass-turbo.eu/s/1AAv)).
* Koulut ([`amenity=school`, `amenity=college`, `amenity=university`](https://overpass-turbo.eu/s/1AAt)).
* Ostoskeskukset ([`shop=mall`](https://overpass-turbo.eu/s/1AAF)).
* Sairaalat ([`amenity=hospital`](https://overpass-turbo.eu/s/1AAC)).
* Terveyskeskukset (ja muut terveyspalveluita tuottavat tahot) ([`amenity=clinic`, `healthcare=clinic`](https://overpass-turbo.eu/s/1AAD)).
* Päivittäistavarakaupat ([`shop=convenience`, `shop=supermarket`](https://overpass-turbo.eu/s/1AAz)).

Näissä pienoisena ongelmana on se, että nämä vaativat jonkun
OpenStreetMapista erillisen tietokannan kirjaamaan sen, että mitä
paikkoja on läpikäyty. OpenStreetMapia kun ei juurikaan tule käyttää
maastossa olemattoman tiedon säilömiseen. Tällaisen ylläpito
käsin on hieman isompi urakka kuin mitä maastosta löytyvien
paikkapisteiden puutteiden korjaaminen vaatii.

Erillisten tietokantojen synkronointi monen eri laitteen välillä on
myös hieman haastavaa. Lähes kaikki läpikäymäni karttapalvelut eivät
toimi kunnolla kuin selaimella. Kuitenkin tavallisesti käyttämäni
OsmAnd näyttäisi tarjoavan parhaat toiminnot kirjausominaisuuksien
osalta. OsmAndissa voi tuoda, muokata ja viedä GPX-muotoisia pisteitä,
joiden avulla voi kirjata menemisiään. Tällä ominaisuudella voisi
kuvitella saavan aikaan ainakin suhteellisen helposti päivitettävän
henkilökohtaisen tietokannan läpikäydyistä alueista pienellä
ohjelmallisella datankäsittelyllä.

Omaa sijaintiaan nauhoittamalla ja vertaamalla läpikäytävään dataan
voisi myös saada kohtuullisen heuristiikan siitä, että mitä on tullut
läpikäytyä.

#### Osaksi kartoitettujen asuinalueiden läpikäynti

Kun näitä eri paikkoja kävi läpi, välillä tuli vastaan tapauksia,
joissa yhdessä korttelissa oli pyörätelineet OpenStreetMapissa, mutta
ympäröivissä kortteleissa ei ollut. Näitä jos lähtee kartalta
selailemaan, löytää todennäköisesti alueilta, joilta voi löytyä lisää
OpenStreetMapista puuttuvia pyöräparkkeja.

<figure id="potentiaalinen-pyöräpysäköintialue">
<a href="https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/kalasatama-potentiaalinen-alue-pyöräpysäkeille.webp"><img
src="https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/kalasatama-potentiaalinen-alue-pyöräpysäkeille.webp"
height="600"
alt="Kalasataman kortteleita, joissa osassa on sinisin neliöin
merkittyjä pyöräpysäkkejä kartalla."
title="Kalasataman kortteleita, joissa osassa on sinisin neliöin
merkittyjä pyöräpysäkkejä kartalla."
/></a>
<figcaption>
Kuva 3: parin umpikorttelin alueelta löytyvät pyöräparkit (siniset
neliöt) vihjailevat, että ympäristöä kannattaisi käydä läpi
lisämateriaalin toivossa.
</figcaption>
</figure>

[Kuvassa 3](#potentiaalinen-pyöräpysäköintialue) on esimerkki
Kalasatamasta, jossa muutamasta korttelista löytyy kasoittain
pyörätelineitä, mutta näiden ympäristössä on kasoittain kortteleita,
joista ei ole yhtään telinettä kartalla. On mahdollista, että näissä
tosiaan ei ole telineitä kartoitettavaksi, mutta on todennäköisempää,
että telineitä vain ei ole lisätty kartalle. Näiden mahdollisten
pyörätelineitä sisältävien alueiden löytäminen vaatii vähän vaivaa,
sillä OpenStreetMapista puuttuvaa dataa on hieman hankala löytää
automaattisin työkaluin.

## Overpass-kysely

Lopuksi vielä tuodaan esiin pääasiallinen apuväline näiden puutteiden
metsästämisessä. Se on
[Overpass-kysely](https://overpass-turbo.eu/s/1AB0), jolla nämä
kapasiteetin suhteen täydennystä kaipaavat pyöräparkit löytyy. Se
näyttää seuraavanlaiselta:

<pre><code>node
  ["amenity"="bicycle_parking"]["capacity"!~".*"]["access" !~ "private"]["access" !~ "permit"]["access" !~ "no"]["bicycle_parking" !~ "building"]["bicycle_parking" !~ "floor"]["bicycle_parking" !~ "informal"]["bicycle_parking" !~ "shed"]
  (60.13416, 24.67598, 60.26536, 25.1223);
out;
way
  ["amenity"="bicycle_parking"]["capacity"!~".*"]["access" !~ "private"]["access" !~ "permit"]["access" !~ "no"]["bicycle_parking" !~ "building"]["bicycle_parking" !~ "floor"]["bicycle_parking" !~ "informal"]["bicycle_parking" !~ "shed"]
  (60.13416, 24.67598, 60.26536, 25.1223);
(._;>;);
out;</code></pre>

Tämä hakee kaikki [kuvassa 1](#kaikki-pyöräpysäkit-helsinki-bbox)
rajatun alueen pyöräpysäköinnit ja suodattaa niistä sellaiset
näkyviin, jossa ei ole kapasiteettitietoa. Lisäksi tällä tehdään
karsintaa sen osalta, että onko pyöräparkki sellainen, että sinne
pääsee kulkemaan ja suodatetaan pois pyöräparkit, jotka eivät ole
pyörätelinetyyppisiä. [Kuva 4](#tyhjät-pyöräpysäkit-helsinki-bbox)
näyttää sen, miltä tämän kyselyn palauttama data on suhteellisen
loppuvaiheessa näyttänyt.

<figure id="tyhjät-pyöräpysäkit-helsinki-bbox">
<a href="https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/overpass-turbo-tyhjät-pyöräpysäköinnit.webp"><img
src="https://barro.gitlab.io/osm-diary-files/pyöräpysäköintiprojekti-kapasiteettitäydennysten-osalta-pulkassa/overpass-turbo-tyhjät-pyöräpysäköinnit.webp" height="460"
alt="Overpass turbo näyttämässä kapasitietopuutteisia pyöräparkkeja ja
käytetty Overpass-kysely."
title="Overpass turbo näyttämässä kapasitietopuutteisia pyöräparkkeja
ja käytetty Overpass-kysely."
/></a>
<figcaption>
Kuva 4: loppuvaiheen kapasiteettipuutteisia pyöräparkkeja kartalla
läpikäytäväksi ja käytetty kysely.
</figcaption>
</figure>

Jäljellä on tätä kirjoittaessa enää paikkoja, jotka ovat tällä
hetkellä työmaita tai ovat sitten paikassa, jonne ei ole ollut menoa,
kuten Suomenlinna.
