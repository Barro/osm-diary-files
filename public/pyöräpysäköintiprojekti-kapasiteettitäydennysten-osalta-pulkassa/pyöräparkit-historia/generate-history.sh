#!/usr/bin/env bash

set -euo pipefail

mkdir -p data

for minusdays in {978..1}; do
#for minusdays in {320..0}; do
    date=$(date +"%Y-%m-%d" -d "now - $minusdays day" )
    outfile=data/pyöräparkit-helsinki-espoo-$date.geojson
    if [[ -f "$outfile".zst ]]; then
	continue
    fi
    query=$(cat <<EOF
[date:"${date}T02:00:00Z"];
node
  ["amenity"="bicycle_parking"]["access" !~ "private"]["access" !~ "permit"]["access" !~ "no"]["bicycle_parking" !~ "building"]["bicycle_parking" !~ "floor"]["bicycle_parking" !~ "informal"]["bicycle_parking" !~ "shed"]
  (60.13416, 24.67598, 60.26536, 25.1223);
out;
way
  ["amenity"="bicycle_parking"]["access" !~ "private"]["access" !~ "permit"]["access" !~ "no"]["bicycle_parking" !~ "building"]["bicycle_parking" !~ "floor"]["bicycle_parking" !~ "informal"]["bicycle_parking" !~ "shed"]
  (60.13416, 24.67598, 60.26536, 25.1223);
out;
(._;>;);
>;
out body;
EOF
)
    echo "$outfile"
    curl -o pyöräparkit-helsinki-espoo-latest.osm -d @- -X POST https://overpass-api.de/api/interpreter <<< "$query" || continue
    python ../convert-osm-to-geojson.py pyöräparkit-helsinki-espoo-latest.osm "$outfile"
    zstd < "$outfile" > "$outfile".zst
    rm "$outfile"
    ../jq.sh <(zstdcat "$outfile".zst)
    sleep_time=$(( RANDOM % 3 )).$(( RANDOM % 10 ))
    sleep "$sleep_time"
done
