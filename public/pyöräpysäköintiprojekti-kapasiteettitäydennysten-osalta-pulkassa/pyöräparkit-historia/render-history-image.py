#!/usr/bin/python3

import csv
import datetime
import matplotlib.pyplot as plt
import numpy as np

with open("dated-totals.csv") as fp:
    data = list(csv.reader(fp, delimiter=","))

labels = []
capacity_per_type = {
    "stands": [],
    "wall_loops": [],
    "two-tier": [],
    "rack": [],
    "safe_loops": [],
    "other": [],
}
for items in data:
    date = items[0]
    (total_features,
     no_info,
     c_stands,
     c_wall_loops,
     c_two_tier,
     c_rack,
     c_safe_loops,
     c_other) = [int(x) for x in items[1:]]
    labels.append(date)
    capacity_per_type["stands"].append(c_stands)
    capacity_per_type["wall_loops"].append(c_wall_loops)
    capacity_per_type["two-tier"].append(c_two_tier)
    capacity_per_type["rack"].append(c_rack)
    capacity_per_type["safe_loops"].append(c_safe_loops)
    capacity_per_type["other"].append(c_other)

colors = [
    "tab:brown",
    "tab:pink",
    "tab:grey",
    "tab:red",
    "tab:orange",
    "tab:blue",
]

fig, ax = plt.subplots()
fig.set_figwidth(10)
ax.stackplot(
    labels,
    list(reversed(capacity_per_type.values())),
    labels=list(reversed(capacity_per_type.keys())),
    colors=colors,
    alpha=0.8,
)

plt.xticks(
    ["2021-01-01",
     "2021-07-01",
     "2022-01-01",
     "2022-07-01",
     "2023-01-01",
     "2023-07-01",
     ],
    ["2021\nTammikuu",
     "2021\nHeinäkuu",
     "2022\nTammikuu",
     "2022\nHeinäkuu",
     "2023\nTammikuu",
     "2023\nHeinäkuu",
     ])

ax.legend(reversed(plt.legend().legendHandles), capacity_per_type.keys(), loc='upper left')

ax.set_title('Pyöräpysäköintipaikkojen kehitys')
#ax.set_xlabel('Päivä')
ax.set_ylabel('Paikkojen lukumäärä')
ax.set_xlim(left=0, right=len(labels) - 1)

now = datetime.datetime.now() - datetime.timedelta(days=1)
plt.savefig(f"pyöräparkit-määrät-20210101-{now.strftime('%Y%m%d')}.png", dpi=300)
