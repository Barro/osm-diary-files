#!/usr/bin/env bash

set -euo pipefail

for I in {1200..0}; do
    date=$(date +"%Y-%m-%d" -d "now - $I day")
    infile=data/pyöräparkit-helsinki-espoo-"$date".geojson.zst
    if ! [[ -f $infile ]]; then
	continue
    fi
    
    (
	set -euo pipefail
	
	echo -n "$date, "
	python generate-csv.py <(zstdcat "$infile")
    ) | tee -a dated-totals.csv
done
