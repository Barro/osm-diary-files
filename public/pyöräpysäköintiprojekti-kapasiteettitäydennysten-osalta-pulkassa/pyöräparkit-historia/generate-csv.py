import argparse
import json
import sys

parser = argparse.ArgumentParser()
parser.add_argument("infile", type=argparse.FileType())
args = parser.parse_args()
data = json.load(args.infile)
types_capacities = {}
no_info = 0
for item in data["features"]:
    bp_type = item["properties"].get("bicycle_parking")
    if not bp_type:
        no_info += 1
        continue
    bp_capacity = item["properties"].get("capacity")
    if not bp_capacity:
        no_info += 1
        continue
    try:
        bp_capacity = int(bp_capacity)
    except:
        no_info += 1
        continue
    if bp_type not in types_capacities:
        types_capacities[bp_type] = 0
    types_capacities[bp_type] += bp_capacity

total = sum(types_capacities.values())
c_stands = types_capacities.get("stands", 0)
c_rack = types_capacities.get("rack", 0)
c_wall_loops = types_capacities.get("wall_loops", 0)
c_two_tier = types_capacities.get("two-tier", 0)
c_safe_loops = types_capacities.get("safe_loops", 0)
c_other = total - c_stands - c_rack - c_wall_loops - c_two_tier - c_safe_loops
print("%d, %d, %d, %d, %d, %d, %d, %d" % (
    len(data["features"]),
    no_info,
    c_stands,
    c_wall_loops,
    c_two_tier,
    c_rack,
    c_safe_loops,
    c_other))
